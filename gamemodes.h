#ifndef GAMEMODES_H_INCLUDED
#define GAMEMODES_H_INCLUDED

void pauseGame(GameState* state);
void resumeGame(GameState* state);
void quitGame(GameState* state);
void goToSettings(GameState* state);
void goToPauseMenu(GameState* state);
void goToControlsMenu(GameState* state);

#endif
