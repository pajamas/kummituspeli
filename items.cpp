#include "structs.h"
#include "inventory.h"

bool mouseOnGroundItem(StackOfItems item, GameState* state) {
    if (item.item == NULL || item.onLevel != state->currentLevel) return false;

    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);

    float width = bitmap_width(item.item->image);
    float height = bitmap_height(item.item->image);
    float x = item.x;
    float y = item.y + item.yOffset;

    return (mstate.x > x &&
            mstate.x < x + width &&
            mstate.y > y &&
            mstate.y < y + height);
}

void pickUpGroundItem(GameState* state, StackOfItems item) {
    if (item.item == NULL || item.onLevel != state->currentLevel) return;
    int amountAdded = addToInventory(&state->inventory, item.item, item.amount);             
    if (amountAdded < item.amount) {                                                
        item.amount -= amountAdded;
        return;
    }
    // remove from state->itemsOnGround array
    FOR(state->itemsOnGroundNum) {
        if (state->itemsOnGround[i].item == item.item &&
            state->itemsOnGround[i].amount == item.amount) { 
            state->itemsOnGround[i].item = NULL;
            state->itemsOnGround[i].amount = 0;
        }
    }
}

void drawItemOnGround(StackOfItems item, GameState* state) {
    if (item.item == NULL || item.onLevel != state->currentLevel) return;
    al_draw_bitmap(item.item->image, item.x, item.y + item.yOffset, 0);
    if (item.amount == 1) return;
    al_draw_text(state->fonts.font, state->colors.textColor, item.x + 20,
                 item.y + item.yOffset, ALLEGRO_ALIGN_LEFT, 
                 to_string(item.amount).c_str());
}

void dropItem(StackOfItems item, GameState* state) {
    if (item.item == NULL) return;

    int p = -1;
    FOR(MAX_GROUND_ITEMS) {
        if (state->itemsOnGround[i].item == NULL) {
            p = i;
            break;
        }
    }
    if (p == -1) return;

    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);

    item.x = mstate.x;
    item.y = mstate.y;
    item.onLevel = state->currentLevel;
    state->itemsOnGround[p] = item;
    if (p + 1 >= state->itemsOnGroundNum) {
        state->itemsOnGroundNum += 1;
    }

    state->handItem.item = NULL;
    state->handItem.amount = 0;

}

void calcItemPositions(GameState* state) {
    if (state->gameMode != OUTSIDE) return;
    FOR(state->itemsOnGroundNum) {
        if (state->itemsOnGround[i].item == NULL ||
            state->itemsOnGround[i].onLevel != state->currentLevel) {
            continue;
        }
        double itemImgHeight = al_get_bitmap_height(state->itemsOnGround[i].item->image);
        if (state->itemsOnGround[i].y < state->groundLevel - itemImgHeight - 25) {
            state->itemsOnGround[i].y += state->itemsOnGround[i].dy;
            state->itemsOnGround[i].dy += GRAVITY;
            return;
        } else {
            state->itemsOnGround[i].y = state->groundLevel - itemImgHeight - 25;
        }
        state->itemsOnGround[i].pos += 0.025;
        state->itemsOnGround[i].yOffset = sin(state->itemsOnGround[i].pos) * 18;
    }

}
