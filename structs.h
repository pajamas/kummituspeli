#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <stdbool.h>
#include <stdio.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <array>
#include <vector>
#include <memory>
#include <cereal/archives/json.hpp>
#include <cereal/types/memory.hpp>

#define TREES_AMOUNT 22
#define PLANTS_AMOUNT 60
#define PLANT_SPECIES_NUM 8
#define FLOWER_POT_STAGES_NUM 5
#define INVENTORY_SIZE 8
#define INVENTORY_PADDING 9
#define CHEST_SIZE 27
#define MAX_INVENTORY_SIZE 27
#define MAX_LEVEL_ENTITIES 20
#define INVENTORY_SLOT_WIDTH 45
#define PLANTABLE_SPECIES 3
#define MAX_CAVE_SIZE 729
#define MAX_BUBBLES 50
#define WHITE al_map_rgb(255,255,255)
#define GEMS 4
#define MAX_BATS 3
#define MAX_SPIDERS 4
#define MAX_COMMAND_LENGTH 50
#define MAX_GROUND_ITEMS 100
#define GRAVITY 0.4
#define MINERALS_AMOUNT 2

#define FOR(num) for(int i = 0; i < num; i++)
#define FORI(num) for(int i = 0; i < num; i++)
#define FORJ(num) for(int j = 0; j < num; j++)

#define bitmap(s) al_load_bitmap(s)
#define bitmap_height(s) al_get_bitmap_height(s)
#define bitmap_width(s) al_get_bitmap_width(s)

using std::string;
using std::to_string;
using std::array;
using std::vector;

struct tetrisState;
typedef struct gameState GameState;

typedef struct vector2d {
    double x,y;
} Vector2d;

enum direction { UP, RIGHT, DOWN, LEFT, ROTATE };

typedef struct Item Item;

class Entity {
    public:
        string type;
        string dialogue;
        double x,y;
        double width, height;
        double dx, dy, speed, attackSpeed, jumpHeight;
        double health, maxHealth, damageAmount;
        double xOffset, yOffset, pos;
        ALLEGRO_BITMAP *image, *normalImage, *damagedImage, *deadImage, *asleepImage, 
                   *harvestedImage;
        ALLEGRO_BITMAP* icon;
        bool showingDialogue, showing, clickable, pickedUp, pickupable, 
            feedable, flipped, jumping, open, killable, dead, attacking, happy, angry,
            tameable, tamed, likesAllFood, alwaysJumping, asleep, harvestable;
        Item* favoriteFood;
        int growthStage, dialogueStageNum, speciesID, ID;
        Item *item, *hat;
        void (*onclick)(GameState*);
        ALLEGRO_TIMER* dialogueTimer;
        ALLEGRO_TIMER* damageTimer;
        ALLEGRO_TIMER* attackCooldown;
        ALLEGRO_TIMER* emoteTimer;
        Entity* drop;
        ALLEGRO_SAMPLE* sound;
        int onLevel;

        char caveSymbol;
        
        void loadImage(string pathToImg);
        void makeSize();

        void setOnClick(void (*function)(GameState*));

        bool hasDialogue();
        void makeDialogue(string text, GameState* state);
        void showDialogue();

        void changeImage(ALLEGRO_BITMAP* image);
        bool hover(GameState* state);
        void click(GameState* state);

        void draw(GameState* state);
        void drawAtCoords(double x, double y, GameState* state);
        void drawImage(GameState* state);
        void drawHPBar(GameState* state);
        void drawHitbox(GameState* state);

        void damage(double amount, GameState* state);

        void jump(double groundLevel);

        void becomeHappy();
        void becomeAngry();
        void pickUp(GameState* state);
        void harvest(GameState* state);

        void makeX(double xInLevel);
        double xOnScreen(double viewX);

        void addToLevel(GameState* state);
        void removeFromLevel();

        template<class Archive>
        void serialize(Archive & archive) {
            archive( x, y );
        }
};

class Ghost: public Entity {
    public:
        void move(GameState* state);
        Ghost() {
            type = "Ghost";
        }
};

class Animal: public Entity {
    public:
    bool feedable, tameable, tamed, likesAllFood, asleep;
    void feed(Item* food, GameState* state);
    void fallAsleep();
    void followGhost(GameState* state);
    void move(GameState* state);
    void click(GameState* state);
    void addToLevel(GameState* state);

    Animal(string type, int onLevel) {
        this->type = type;
        this->onLevel = onLevel;
    }
};

class Customer: public Entity {
    public:
    ALLEGRO_BITMAP* portrait;
    bool doneShopping;

    string greeting, thankyou;

    void init() {
        x = 0;
        y = 0;
        yOffset = 0;
        xOffset = 0;
        dy = 0;
        speed = 2;
        type = "Customer";
        width = 0;
        height = 0;
        happy = false;
        angry = false;
        dialogue = "";
        greeting = "";
        thankyou = "";
        showingDialogue = false;
        flipped = false;
        doneShopping = false;
    }
    Customer() {
        init();
    }
    Customer(int X) {
        init();
        x = X;
    }

    void talk(GameState* state);
};

class Building: public Entity {
    public:

    double doorX, doorY, doorWidth, doorHeight;

    Building(string type, int onLevel) {
        this->type = type;
        this->onLevel = onLevel;
    }
};

class Furniture: public Entity {
    public:

};

typedef struct plant {
    double x,y;
    int number;
    ALLEGRO_BITMAP* img;
} Plant;

typedef struct tree {
    double x, y;
    int height;
    ALLEGRO_BITMAP* img;
} Tree;

typedef struct Item {
    string name;
    int ID, flowerID;
    bool isFlower, isFood, isGem, nonStackable, isMineral;
    ALLEGRO_BITMAP* image;
    ALLEGRO_BITMAP* bookPageImage;
    int buyPrice, sellPrice;
    void (*onclick)(GameState*);

    Item();
    Item(std::string name, std::string imagePath, GameState* state);
    Item(std::string name, ALLEGRO_BITMAP* image, GameState* state);

    void setPrices(int buyPrice, int sellPrice);
} Item;

typedef struct stackOfItems {
    Item* item;
    int itemID;
    int amount;
    Entity* slot;

    double x, y, dy, yOffset, pos;
    int onLevel;

    bool notOwned;
    template<class Archive>
    void serialize(Archive & archive) {
        if (item != NULL) {
            this->itemID = item->ID;
        } else {
            this->itemID = -1;
        }
        archive( CEREAL_NVP(itemID), CEREAL_NVP(amount) );
    }
} StackOfItems;

typedef struct inventorySlot {
    double x, y, width, height;
    StackOfItems itemsContained;
    template<class Archive>
    void serialize(Archive & archive) {
        archive( CEREAL_NVP(itemsContained) );
    }
} InventorySlot;

typedef struct inventory {
    InventorySlot slots[MAX_INVENTORY_SIZE];
    double x,y;
    int size;
    int width, height;
    int widthPx, heightPx;
    int padding;
    int slotWidth;
    ALLEGRO_COLOR bgColor, slotBgColor;
    bool open;

    void loadItems(GameState* state);

    template<class Archive>
    void serialize(Archive & archive) {
        archive( CEREAL_NVP(slots) );
    }
} Inventory;

typedef struct button {
    double x,y;
    ALLEGRO_BITMAP* image;
    double width, height;
    bool disabled;
    ALLEGRO_COLOR bgColor, activeBgColor, disabledBgColor;
    string text;
    void (*command)(GameState*);
} Button;

typedef struct shopMenu {
    Inventory buyInventory;
    Inventory sellInventory;
    Inventory shopItems;
    int buyPrice;
    int sellPrice;
    Button okButton, cancelButton, trashButton;
    bool open;
    double x,y;
    double width, height;
    ALLEGRO_COLOR bgColor;
    int padding, paddingTop;
    int buyAmount;
} ShopMenu;

typedef struct craftMenu {
    Inventory result;
    Button okButton, cancelButton;
    bool open;
    double x,y;
    double width, height;
    ALLEGRO_COLOR bgColor;
    int padding, paddingTop;
} CraftMenu;

class Menu { // contains bg/layout info, text and buttons
    public:
    double x,y;
    ALLEGRO_BITMAP* bgImage;
    double width, height;
    double padding, paddingTop;
    ALLEGRO_COLOR bgColor;
    Button* buttons[10];
    string text;
    string rows[10];
    int rowsNum;
};
/*
class Menu {
public:
    double x,y;
    double width, height;
    double padding, paddingTop;
    ALLEGRO_COLOR bgColor;
    ALLEGRO_BITMAP* bgImage;

}

class PauseMenu: public Menu {
public:
    Button* buttons[10];
    string text;
    int rowsNum;

};

class CraftMenu: public Menu {
public:
    Inventory result;
    Button okButton, cancelButton;
    bool open;

};
*/
typedef struct bubble {
    double x,y;
    double dx,dy;
    double damage;
    double speed, distanceTraveled;
    bool alive;
    ALLEGRO_BITMAP* image;
} Bubble;

enum modes { PAUSED, OUTSIDE, INSIDE, GREENHOUSE, TETRIS, CAVE, COMMAND, DIALOGUE };
enum menus { M_PAUSED, M_SETTINGS, M_CONTROLS, M_TETRIS_PAUSED, M_TETRIS_OVER };

typedef struct farmablePlant {
    string name;
    ALLEGRO_BITMAP* image;
    Item* item;
} FarmablePlant;

typedef struct fieldTile {
    int growthStage;
    FarmablePlant* plant;
} FieldTile;

typedef struct colors {
    ALLEGRO_COLOR skyColor, darkerSkyColor, groundColor, textColor, 
                  menuBgColor, menuBgColorTransparent,
                  buttonBg, buttonActiveBg, buttonDisabledBg,
                  inventoryBg, inventorySlotBg;
} Colors;

typedef struct images {
    ALLEGRO_BITMAP* bat;
    ALLEGRO_BITMAP* spider;
    ALLEGRO_BITMAP* appleTree;
    ALLEGRO_BITMAP* strawberryPlant;
    ALLEGRO_BITMAP *bubbleImg, *damagedSpider, *deadSpider;
    ALLEGRO_BITMAP* emptyFlowerPot, *lanternOn, *lanternOff;
    ALLEGRO_BITMAP* treeImg;
    ALLEGRO_BITMAP* gemItems[GEMS];
    ALLEGRO_BITMAP* gems[GEMS];
    ALLEGRO_BITMAP* inventoryHover;
    ALLEGRO_BITMAP *heart, *cross;
    ALLEGRO_BITMAP *ghostNormal, *ghostLeft, *ghostRight;
    ALLEGRO_BITMAP *plank, *edgePlank, *field;
    ALLEGRO_BITMAP *customer1, *customer1portrait;
} Images;

typedef struct bookState {
    int containedInBook[PLANTABLE_SPECIES];
    bool open;
    int currentPage;
    ALLEGRO_BITMAP* pages[100];
    Entity leftPage, rightPage;
    Button xButton;
    Button leftButton, rightButton;
} BookState;

enum mineralTypes { SILVER, GOLD };

typedef struct mineral {
    string name;
    char symbol;
    ALLEGRO_BITMAP* image;
    Item* item;
} Mineral;

typedef struct cave {
    string shape;
    int width, height;
    double x,y;
    ALLEGRO_BITMAP* caveWallImg;
    Entity bats[MAX_BATS], ghost, gem, spiders[MAX_SPIDERS];
    int tileSize;
    Button backButton;
} Cave;

typedef struct room {
    double x, y, tileSize, hallwayX, hallwayY, floorY;
    int width, height, wallHeight, hallwayWidth, hallwayHeight;
    ALLEGRO_BITMAP *wallTile, *floorTile, *brightWallTile, *brightFloorTile;
    bool lightsOn;
    Entity lantern;
    Entity* furniture[11][6];
} Room;

typedef struct fonts {
    ALLEGRO_FONT* font;  
    ALLEGRO_FONT* fontMedium;
    ALLEGRO_FONT* fontSmall;
    double fontSize;
    double fontSizeMedium;
    double fontSizeSmall;
} Fonts;

typedef struct gameState {
    ALLEGRO_TIMER* timer;
    ALLEGRO_EVENT_QUEUE* queue;
    enum modes gameMode;
    enum modes previousGameMode;
    
    int windowHeight, windowWidth;
    bool done, redraw, canShoot, showingHitboxes, soundOn;

    Entity flowerPot, flowerPot2, flowerPot3, seed, book,
           house, door, house2, greenhouse,
           stars1, stars2, mysha, backArrow, fairy,
           chestEntity, drops[10], strawberryPlant, appleTree,
           craftingTable;
    Animal fox, bee, bunny, cat;
    Ghost ghost, insideGhost;

    vector<Customer> customers;
    Customer* customerShowingDialogue;

    Entity openBook;
    
    Item seedItem, bookItem, flowerItems[PLANTABLE_SPECIES], gemItems[GEMS],
         strawberryItem, apple, coin, tiara, flowerPotItem, flowerPot2Item, flowerPot3Item,
         pumpkinItem, potatoItem;
    std::vector<Item*> allItems;
    StackOfItems handItem;
    int newestItemID;
    int newestEntityID;

    array<Item, MINERALS_AMOUNT> mineralItems;
    array<Mineral, MINERALS_AMOUNT> minerals;

    Inventory inventory, chest;

    std::vector<Entity*> toShow;
    std::vector<Animal*> toShowAnimals;
    std::vector<Building*> toShowBuildings;
    
    std::vector<Animal*> pets;
    int currentLevel;
    double groundLevel;
    Tree trees[TREES_AMOUNT];
    Plant plants[PLANTS_AMOUNT];
    ALLEGRO_BITMAP* plantspng[PLANT_SPECIES_NUM];
    ALLEGRO_BITMAP* flowerPotStages[PLANTABLE_SPECIES][FLOWER_POT_STAGES_NUM];
    ALLEGRO_BITMAP* flowerItemImages[PLANTABLE_SPECIES];
    ALLEGRO_BITMAP* bookPages[PLANTABLE_SPECIES];
    ALLEGRO_TIMER* growTimer;
    ALLEGRO_EVENT event;
    ALLEGRO_TIMER* bubbleTimer;
    unsigned char key[ALLEGRO_KEY_MAX];
    Bubble bubbles[MAX_BUBBLES];
    ALLEGRO_BITMAP* bubbleImg;
    double bubbleSpeed;

    struct tetrisState* tetris;
    ALLEGRO_BITMAP* tetrisBlock;
    ALLEGRO_BITMAP* tetrisBlocks;
    BookState bookState;
    
    Colors colors;
    Images images;
    Fonts fonts;

    Menu escMenu;
    Menu settingsMenu;
    Menu controlsMenu;
    Menu* currentMenu;
    ShopMenu shopMenu;
    CraftMenu craftMenu;

    Room room;
    Cave cave;
    Cave bigCave;

    string commandText;

    Entity* entities[5][MAX_LEVEL_ENTITIES];
    Animal* animals[5][MAX_LEVEL_ENTITIES];
    Building* buildings[5][MAX_LEVEL_ENTITIES];

    ALLEGRO_SAMPLE* bubblePop;
    ALLEGRO_SAMPLE* doorSound;

    StackOfItems itemsOnGround[MAX_GROUND_ITEMS];
    int itemsOnGroundNum;

    double viewX;

    FarmablePlant farmablePlants[2];
    FieldTile fieldTiles[300];
    ALLEGRO_TIMER* fieldGrowTimer;

} GameState;

// button functions
Button newButton(GameState* state, string text);
bool buttonInteract(Button* button, GameState* state);
bool checkForMouseOnButton(Button* button);
void drawButton(GameState* state, Button* button);
// menu functions
Menu newMenu(GameState* state);
void addTextToMenu(Menu* menu, string text);
void menuTextToRows(Menu* menu);
void autoSizeMenu(GameState* state, Menu* menu);
void drawMenu(GameState* state, Menu* menu);
// inventory functions
Inventory newInventory(int width, int height, GameState* state);
// bubble functions
void createBubble(GameState* state, Entity* ghost);
int freeBubbleSlot(Bubble* bubbles);
void drawBubble(Bubble* bubble);
// vector functions
Vector2d newVector2d(double x, double y);
Vector2d vector2dDivide(Vector2d vector, double num);
Vector2d vector2dMultiply(Vector2d vector, double num);
double vector2dLength(Vector2d vector);
Vector2d unitVector2d(Vector2d vector);
// item functions
int newItemID(int* ids);

#endif
