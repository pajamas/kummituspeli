#include "structs.h"
#include "other.h"
#include <iostream>

void spawnCustomer(GameState* state) {
    Customer newCustomer(1000);
    newCustomer.changeImage(state->images.customer1);
    newCustomer.y = state->groundLevel - newCustomer.height;
    newCustomer.showing = true;
    newCustomer.clickable = true;
    newCustomer.onLevel = 1;
    newCustomer.greeting = "Hello!";
    newCustomer.thankyou = "Thanks!";
    newCustomer.dialogue = newCustomer.greeting;
    newCustomer.portrait = state->images.customer1portrait;
    state->customers.push_back(newCustomer);
}

void Customer::talk(GameState* state)  {
    state->customerShowingDialogue = this;
    state->previousGameMode = state->gameMode;
    state->gameMode = DIALOGUE;
}
