#include "structs.h"
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

bool strEquals(char* a, char* b) {
    if (a == NULL && b == NULL) return true;
    if (a == NULL || b == NULL) return false;
    if (strcmp(a,b) == 0) {
        return true;
    }
    return false;
}

void error(string message) {
    const char* msg = message.c_str();
    fprintf(stderr, "Error: %s\n",msg);
    exit(0);
}

void soundToggle(GameState* state) {
    state->soundOn = !state->soundOn;
    if (state->soundOn) {
        state->settingsMenu.buttons[0]->text = "sound off";
    } else {
        state->settingsMenu.buttons[0]->text = "sound on";
    }
}

void playSound(ALLEGRO_SAMPLE* sound, float volume, GameState* state) {
    if (!state->soundOn) return;
    if (sound == NULL) return;
    al_play_sample(sound, volume, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
}

void initTimer(ALLEGRO_TIMER** timer, double t, GameState* state) {
    *timer = al_create_timer(t);
    al_register_event_source(state->queue, al_get_timer_event_source(*timer));
}
