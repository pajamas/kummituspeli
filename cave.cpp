#include "cave.h"
#include "levels.h"
#include "draw.h"
#include "perlin.h"
#include "events.h"
#include "structs.h"
#include "positions.h"
#include "entities.h"
#include "other.h"
#include "inventory.h"

void drawCaveGamemode(GameState* state) {
    Cave* cave = &state->bigCave;
    al_clear_to_color(al_map_rgb(43,36,60));
    // draw floor tiles
    ALLEGRO_BITMAP* tileImg;
    for (int i = 0; i < cave->height; i++) {
        for (int j = 0; j < cave->width; j++) {
            if (cave->shape[i*cave->width + j] != '_') { 
                switch (cave->shape[i*cave->width + j]) {
                    case 's':
                        tileImg = state->minerals[SILVER].image;
                        break;
                    case 'g':
                        tileImg = state->minerals[GOLD].image;
                        break;
                    default:
                        tileImg = cave->caveWallImg;
                        break;
                }
                // distance between ghost and tile
                float d = sqrt( 
                               pow(i+0.5-(cave->ghost.y+cave->ghost.height/2-cave->y)/cave->tileSize,2) 
                             + pow(j+1-(cave->ghost.x + cave->ghost.width-cave->x)/cave->tileSize,2));
                // opacity for light
                float opacity = 0.1 + d / 9;

                // draw light or dark tile based on distance to ghost and
                // whether there is a wall between tile and ghost
                
                // bright tile
                if (d < 5 && !checkForBlockInBetween(cave,j,i,cave->ghost.x,cave->ghost.y)) {
                    al_draw_tinted_bitmap(tileImg, 
                                          al_map_rgba_f(0.85, 0.83, 0.6, opacity), 
                                          cave->x + j*cave->tileSize, 
                                          cave->y + i*cave->tileSize, 0);
                    // draw gem if in light
                    if (xToCaveX(cave,cave->gem.x) == j && yToCaveY(cave,cave->gem.y) == i
                        && !cave->gem.pickedUp) {
                        cave->gem.showing = true;
                        cave->gem.drawImage(state);
                    } else {
                        cave->gem.showing = false;
                    }
                // dark tile
                } else {
                    al_draw_tinted_bitmap(tileImg,
                                          al_map_rgba_f(0.335, 0.33, 0.35, 0.4), 
                                          state->bigCave.x + j*cave->tileSize, 
                                          state->bigCave.y + i*cave->tileSize, 0);
                }
            }
        }
    }


    // up button
    float x1 = state->windowWidth/2;
    float y1 = 30;
    al_draw_filled_triangle(x1,y1,x1-10,y1+17,x1+10,y1+17,state->colors.textColor);

    // entities
    for (Entity* entity: state->toShow) {
        if (entity == NULL) continue;
        entity->draw(state);
    }
     
    // bubbles
    int bubbleSize = al_get_bitmap_width(state->bubbleImg);
    for (Bubble bubble: state->bubbles) {
        if (bubble.alive)
            al_draw_scaled_bitmap(state->bubbleImg, 0, 0, bubbleSize, bubbleSize, 
                                  bubble.x, bubble.y, 20, 20, 0);
    }

    // ghost
    al_draw_scaled_bitmap(state->ghost.image,0,0,state->ghost.width,state->ghost.height,
                          cave->ghost.x,cave->ghost.y,cave->ghost.width,cave->ghost.height,0);

    // ghost hp bar
    if (state->ghost.health == state->ghost.maxHealth) return;

    float barWidth = 120;
    float barHeight = 24; 
        
    float barX = state->windowWidth/2 - barWidth/2;
    float barY = 90;
    float barPadding = 4;
        
    al_draw_filled_rectangle(barX, barY, barX + barWidth, barY + barHeight,
                             state->colors.inventoryBg);
    al_draw_filled_rectangle(barX+barPadding, barY+barPadding, 
                             barX + (state->ghost.health/state->ghost.maxHealth)
                                * barWidth - barPadding,
                             barY + barHeight - barPadding,
                             al_map_rgb(180,73,86));
}

bool checkForBlockInBetween(Cave* cave, int x, int y, int ghostX, int ghostY) {
    ghostX = xToCaveX(cave,ghostX);
    ghostY = yToCaveY(cave,ghostY);

    if (x >= cave->width || y >= cave->height) {
        error("checkForBlockInBetween: x or y is too high");
    }

    int x2,y2;
    for (int i = 1; i <= 5; i++) {
        x2 = ghostX + (x-ghostX)*i/5;
        y2 = ghostY + (y-ghostY)*i/5;
        if (cave->shape[y2*cave->width + x2] == '_') {
            return true;
        }
    }

    return false;
}

void goIntoCave(GameState* state) {
    clearLevel(state);
    state->gameMode = CAVE;

    Cave* cave = &state->bigCave;

    // make cave layout
    int random = rand() % 1000;
    for (int i = 0; i < cave->height; i++) {
        for (int j = 0; j < cave->width; j++) {
            if (perlin2d(i+random,j+random, 0.29, 1) > 0.4) {
                cave->shape[i * cave->width + j] = 'X';
            } else {
                cave->shape[i * cave->width + j] = '_';
            }
            // corners
            if (i + j < state->windowHeight*6/1080 || i + j > state->windowHeight*43/1080) {
                cave->shape[i * cave->width + j] = '_';
            }
            if (i == 0 || j == 0 || j == cave->width-1 || i == cave->height-1) {
                cave->shape[i * cave->width + j] = '_';
            }
        }
    }

    // add ghost
    int n = randomCavePosition(cave->shape);
    Entity* ghost = &cave->ghost;
    ghost->x = cave->x + (n % cave->width) * cave->tileSize;
    ghost->y = cave->y + (n / cave->width) * cave->tileSize;

    // add gem
    int gemN = rand() % GEMS;
    cave->gem.image = state->images.gems[gemN];
    cave->gem.item = &state->gemItems[gemN];
    n = randomCavePosition(cave->shape);
    cave->gem.x = cave->x + (n % cave->width) * cave->tileSize;
    cave->gem.y = cave->y + (n / cave->width) * cave->tileSize;
    cave->gem.xOffset = (cave->tileSize - al_get_bitmap_width(cave->gem.image))/2;
    cave->gem.yOffset = (cave->tileSize - al_get_bitmap_height(cave->gem.image))/2;
    cave->gem.pickedUp = false;
    cave->gem.addToLevel(state);

    // add bats
    for (Entity &bat: cave->bats) {
        addEntityToCave(cave, &bat);
        bat.addToLevel(state);
    }

    // add spiders
    makeSpiders(state);
    FOR(MAX_SPIDERS) {
        addEntityToCave(cave, &cave->spiders[i]);
    }
    FOR(MAX_SPIDERS) {
        calcSpiderPos(cave,i);
        cave->spiders[i].addToLevel(state);
    }

    FOR(2) {
        addMineralToCave(state, cave, GOLD);
        addMineralToCave(state, cave, SILVER);
    }
    
}

void addMineralToCave(GameState* state, Cave* cave, enum mineralTypes mineralType) {
    int pos;
    pos = randomCavePosition(cave->shape);
    if (cave->shape[pos] == 'X') {
        cave->shape[pos] = state->minerals[mineralType].symbol;
    }
}

void addEntityToCave(Cave* cave, Entity* entity) {
    int pos;
    if (entity->type == "Bat") {
        while (true) {
            pos = randomCavePosition(cave->shape);
            if (pos < cave->width || cave->shape[pos-cave->width] == '_') {
                cave->shape[pos] = 'B';
                entity->x = cave->x + (pos % cave->width)*cave->tileSize;
                entity->y = cave->y + floor(pos/cave->width)*cave->tileSize;
                return;
            }   
        }
    }
    else if (entity->type == "Spider") {
        pos = randomCavePosition(cave->shape);
        cave->shape[pos] = 'S';
        return;
    } else {
        return;
    }
}  

int xToCaveX(Cave* cave, float x) {
    float x0 = cave->x;
    int tile = cave->tileSize;
 
    return (int)floor((x-x0)/tile);
}

int yToCaveY(Cave* cave, float y) {
    float y0 = cave->y;
    int tile = cave->tileSize;
 
    return (int)floor((y-y0)/tile);
}

bool caveCollisionCheck(Cave* cave, Entity* entity, enum direction dir) {
    // determine ghost corner positions in grid
    int entityX = xToCaveX(cave,entity->x);
    int entityY = yToCaveY(cave,entity->y);
    int entityX2 = xToCaveX(cave,entity->x+entity->width);
    int entityY2 = yToCaveY(cave,entity->y+entity->height);

    switch (dir) {
        case DOWN:
            if (entityY == cave->height-1) return true;
            if (cave->shape[(entityY+1)*cave->width + (entityX)] == 'X' &&
                cave->shape[(entityY+1)*cave->width + (entityX2)] == 'X') {
                return false;
            }
            break;
        case UP:
            if (entity->y < cave->y+4) return true;
            if (cave->shape[(entityY-1)*cave->width + (entityX)] == 'X' &&
                cave->shape[(entityY-1)*cave->width + (entityX2)] == 'X') {
                return false;
            }
            break;
        case LEFT:
            if (entity->x < cave->x+4) return true;
            if (cave->shape[(entityY)*cave->width + (entityX-1)] == 'X' &&
                cave->shape[(entityY2)*cave->width + (entityX-1)] == 'X') {
                return false;
            }
            break;
        case RIGHT:
            if (entityX == cave->width-1) return true;
            if (cave->shape[(entityY)*cave->width + (entityX+1)] == 'X' &&
                cave->shape[(entityY2)*cave->width + (entityX+1)] == 'X') {
                return false;
            }
            break;
        default:
            break;
    }

    float tiley,tilex;
    float ey,ex,diff;

    // if tile is wall, return true
    
    if (dir == DOWN) {
        // coordinates of ghosts feet
        ey = entity->y + entity->height;
        // coordinates of tile below ghosts feet
        tiley = cave->y + (entityY2+1)*cave->tileSize;
        // difference
        diff = tiley-ey;
        if (cave->shape[(entityY+1)*cave->width + (entityX)] == '_' && diff < 4) {
            return true;
        }
        if (cave->shape[(entityY+1)*cave->width + (entityX2)] == '_' && diff < 4) {
            return true;
        }
        return false;
    }
    else if (dir == UP) {
        // coordinates of ghosts head
        ey = entity->y;
        // coordinates of tile above ghosts head
        tiley = cave->y + (entityY)*cave->tileSize;
        // difference
        diff = ey-tiley;
        if ((cave->shape[(entityY-1)*cave->width + (entityX)] == '_' && diff < 4) || 
            (cave->shape[(entityY-1)*cave->width + (entityX2)] == '_' && diff < 4)) {
            return true;
        }
        return false;
    }
    else if (dir == RIGHT) {
        // coordinates of ghosts right side
        ex = entity->x + entity->width;
        // coordinates of tile on the right of ghost
        tilex = cave->x + (entityX2+1)*cave->tileSize;
        // difference
        diff = tilex-ex;
        if ((cave->shape[(entityY)*cave->width + (entityX+1)] == '_' && diff < 4) || 
            (cave->shape[(entityY2)*cave->width + (entityX+1)] == '_' && diff < 4)) {
            return true;
        }
        return false;
    } else {
        // coordinates of ghosts left side
        ex = entity->x;
        // coordinates of tile on the left of ghost
        tilex = cave->x + (entityX)*cave->tileSize;
        // difference
        diff = ex-tilex;
        if ((cave->shape[(entityY)*cave->width + (entityX-1)] == '_' && diff < 4) || 
            (cave->shape[(entityY2)*cave->width + (entityX-1)] == '_' && diff < 4)) {
            return true;
        }
        return false;
    }
}

int freeCavePosition(string shape) {
    FOR(MAX_CAVE_SIZE) {
        if (shape[i] == 'X') {
            return i;
        }
    }
    return -1;
}

int randomCavePosition(string shape) {
    int pos;
    while(true) {
        pos = rand() % MAX_CAVE_SIZE;
        if (shape[pos] == 'X') {
            return pos;}
    }
}

void leaveCave(GameState* state) {
    goOutside(state);
}

void makeSpiders(GameState* state) {
    // how many spiders
    int amount = rand() % (MAX_SPIDERS - 1) + 1;
    

    FOR(amount) {
        state->bigCave.spiders[i] = newSpider(state);
        state->bigCave.spiders[i].ID = newEntityID(state);
        al_register_event_source(state->queue, 
                                 al_get_timer_event_source(state->bigCave.spiders[i].damageTimer));
        al_register_event_source(state->queue, 
                                 al_get_timer_event_source(state->bigCave.spiders[i].attackCooldown));
    }
}

void bubbleCaveCollisions(GameState* state, Bubble* bubble) {
    char* tile = &state->bigCave.shape[yToCaveY(&state->bigCave,bubble->y+10)*
            state->bigCave.width + xToCaveX(&state->bigCave,bubble->x+10)];
        if (*tile == '_') {
            bubble->alive = false;
        } else if (*tile == 'g' || *tile == 's') {
            bubble->alive = false;
            enum mineralTypes mineralType;
            switch (*tile) {
                case 'g':
                    mineralType = GOLD;
                    break;
                case 's':
                    mineralType = SILVER;
                    break;
                default:
                    return;
            }
            addToInventory(&state->inventory, &state->mineralItems[mineralType], 3);
            *tile = 'X';
        }
        for (Entity &spider: state->bigCave.spiders) {
            if (spider.dead) continue;
            if (bubbleEntityCollision(bubble, &spider)) {
                bubble->alive = false;
                spider.damage(state->ghost.damageAmount, state);
            }
        }
}
