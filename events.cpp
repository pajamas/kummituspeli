#include "structs.h"
#include "positions.h"
#include "inventory.h"
#include "levels.h"
#include "interactions.h"
#include "events.h"
#include "tetrislogic.h"
#include "gamemodes.h"
#include "cave.h"
#include "chest.h"
#include "other.h"
#include "entities.h"
#include "house.h"
#include "command.h"
#include "shopmenu.h"
#include "items.h"
#include "crafting.h"

#define KEY_SEEN     1
#define KEY_RELEASED 2

#define ghostInsideSpeed 5.5

void handleEvents(GameState* state) {
    switch(state->event.type) {
        case ALLEGRO_EVENT_TIMER:
            FOR(MAX_SPIDERS) {
                if (state->event.timer.source == state->bigCave.spiders[i].damageTimer) {
                    al_stop_timer(state->bigCave.spiders[i].damageTimer);
                    if (!state->bigCave.spiders[i].dead) {
                        state->bigCave.spiders[i].image = state->bigCave.spiders[i].normalImage;
                    }
                }
                if (state->event.timer.source == state->bigCave.spiders[i].attackCooldown) {
                    al_stop_timer(state->bigCave.spiders[i].attackCooldown);
                }
            }
            for (Entity* entity: state->toShow) {
                if (entity == NULL) continue;
                if (state->event.timer.source == entity->emoteTimer) {
                    al_stop_timer(entity->emoteTimer);
                    entity->happy = false;
                    entity->angry = false;
                    return;
                }
                else if (state->event.timer.source == entity->dialogueTimer) {
                    al_stop_timer(entity->dialogueTimer);
                    entity->showingDialogue = false;
                }
            }
            if (state->event.timer.source == state->bubbleTimer) {
                al_stop_timer(state->bubbleTimer);
            }
            else if (state->event.timer.source == state->growTimer) {
                grow(state);
            }
            else if (state->event.timer.source == state->tetris->tetrisTimer) {
                tetrisTick(state);
            }
            else {

            for (int i = 0; i < ALLEGRO_KEY_MAX; i++)
            state->key[i] &= KEY_SEEN;

            for (Entity* entity: state->toShow) {
                if (entity == NULL) continue;
                entity->jumping = false;
            }

            if (state->canShoot) {
                FOR(MAX_BUBBLES) {
                    Bubble* bubble = &state->bubbles[i];
                    // kill bubbles outside view
                    if (bubble->x > state->windowWidth + 50 ||
                        bubble->x < -50 ||
                        bubble->y > state->windowHeight + 50 ||
                        bubble->y < -50) {
                        bubble->alive = false;
                    }
                    if (state->gameMode == CAVE) {
                        bubbleCaveCollisions(state, bubble);    
                    }
                    // move alive bubbles
                    if (bubble->alive) {
                        bubble->x += bubble->dx;
                        bubble->y += bubble->dy;
                    }
                }
            }
            if (state->canShoot && state->key[ALLEGRO_KEY_SPACE] && 
                        !al_get_timer_started(state->bubbleTimer)) {
                Entity* ghost = &state->ghost;
                if (state->gameMode == CAVE)
                    ghost = &state->bigCave.ghost;
                
                createBubble(state, ghost);
                al_start_timer(state->bubbleTimer);
            }
            // outside stuff
            if (state->gameMode == OUTSIDE) {
                if (state->key[ALLEGRO_KEY_LEFT] | state->key[ALLEGRO_KEY_A]) {
                    if (state->ghost.x - state->viewX < 
                            state->windowWidth/3 &&
                        state->viewX > 0) {
                        state->viewX -= state->ghost.speed;
                    }
                    state->ghost.x -= state->ghost.speed;
                    state->ghost.image = state->images.ghostLeft;
                }
                if (state->key[ALLEGRO_KEY_RIGHT] | state->key[ALLEGRO_KEY_D]) {
                    if (state->ghost.x - state->viewX > 
                            state->windowWidth*2/3) {
                        state->viewX += state->ghost.speed;
                    }
                    state->ghost.x += state->ghost.speed;
                    state->ghost.image = state->images.ghostRight;
                }
                if (state->bee.showing) calcBeePos(state);
                if (state->seed.showing) calcSeedPos(state);
                if (state->fox.showing) state->fox.followGhost(state);
                calcItemPositions(state);

                // follow ghost, jumping, falling
                for (Animal* animal: state->toShowAnimals) {
                    animal->move(state);
                }
                state->ghost.move(state);

                if (state->ghost.x < -30) { 
                    if (state->currentLevel==1){
                        state->ghost.x = 30;
                        state->ghost.y = state->groundLevel-100;
                        state->ghost.dy = 0;}
                }
                if (state->ghost.x > 
                   (state->currentLevel)*1920) {
                    state->currentLevel += 1;
                    state->ghost.onLevel += 1;
                    genNextLevel(state);
                } else if (state->currentLevel > 1 && 
                           state->ghost.x <
                          (state->currentLevel-1)*1920) {
                    state->currentLevel -= 1;
                    state->ghost.onLevel -= 1;
                    genPreviousLevel(state);
                }

                // move the stars
                state->stars1.y += 0.5;
                state->stars2.y += 0.5;
                if (state->stars1.y >= 600){
                    state->stars1.y = -600;
                }
	            if (state->stars2.y >= 600) {
                    state->stars2.y = -600; 
                }

            }    
             
            else if (state->gameMode == INSIDE) { // inside house stuff
                moveInRoom(state);
	        }
            // cave stuff
            else if (state->gameMode == CAVE) {
                Entity* ghost = &state->bigCave.ghost;
                if (state->key[ALLEGRO_KEY_LEFT] | state->key[ALLEGRO_KEY_A])
                    if (!caveCollisionCheck(&state->bigCave, ghost, LEFT)) {
                        ghost->x -= 3; 
                    }
                if (state->key[ALLEGRO_KEY_RIGHT] | state->key[ALLEGRO_KEY_D])
                    if (!caveCollisionCheck(&state->bigCave, ghost, RIGHT)) {
                        ghost->x += 3; 
                    }
                if (state->key[ALLEGRO_KEY_DOWN] | state->key[ALLEGRO_KEY_S])
                    if (!caveCollisionCheck(&state->bigCave, ghost, DOWN)) {
                        ghost->y += 3; 
                    }
                if (state->key[ALLEGRO_KEY_UP] | state->key[ALLEGRO_KEY_W])
                    if (!caveCollisionCheck(&state->bigCave, ghost, UP)) {
                        ghost->y -= 3; 
                    }
                FOR(MAX_SPIDERS) {
                        calcSpiderMove(&state->bigCave.spiders[i],
                                       &state->bigCave.ghost,
                                       &state->bigCave, state);
                }
            }

	        state->redraw = true;}
            break;

        case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
            if (state->gameMode == DIALOGUE) {
                resumeGame(state);
                return;
            }
            if (state->gameMode != PAUSED) {
                if (state->inventory.open) {
                    if (inventoryInteract(state, &state->inventory, state->event.mouse.button))
                        return;
                }
                if (state->chest.open) {
                    if (inventoryInteract(state, &state->chest, state->event.mouse.button))
                        return;
                }
                if (state->shopMenu.open) {
                    if (shopMenuInteract(state, state->event.mouse.button)) return;
                }
            }
            if (state->event.mouse.button == 1) {
                if (state->gameMode == CAVE &&
                    checkForMouseOnButton(&state->bigCave.backButton)) {
                    state->bigCave.backButton.command(state);
                    return;
                }
                Menu* menu = state->currentMenu;
                if (menu != NULL) {
                    for (int i = 0; i < 10; i++) {
                        if (checkForMouseOnButton(menu->buttons[i])) {
                            if (menu->buttons[i]->command != NULL &&
                                    !menu->buttons[i]->disabled) {
                                menu->buttons[i]->command(state);
                                break;
                            }
                            return;
                        }
                    }
                    return;
                }
                whatClicked(state);
            } else if (state->event.mouse.button == 2) {

            }
            break;

        case ALLEGRO_EVENT_KEY_DOWN:
            state->key[state->event.keyboard.keycode] = KEY_SEEN | KEY_RELEASED;
            if (state->gameMode == OUTSIDE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_LEFT ||
                        state->event.keyboard.keycode == ALLEGRO_KEY_A) {
                    state->ghost.image = state->images.ghostLeft;
                }
                else if (state->event.keyboard.keycode == ALLEGRO_KEY_RIGHT ||
                        state->event.keyboard.keycode == ALLEGRO_KEY_D) {
                    state->ghost.image = state->images.ghostRight;
                }
            }
            break;
    
        case ALLEGRO_EVENT_KEY_CHAR:
            if (state->event.keyboard.keycode == ALLEGRO_KEY_F1)
                state->showingHitboxes = !state->showingHitboxes;
            if (state->gameMode == COMMAND) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                    closeCommandLine(state);
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_BACKSPACE) {
                    if (state->commandText.length() > 0) {
                        state->commandText.pop_back();
                    }
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_ENTER) {
                    state->commandText += '\n';
                    parseCommand(state);
                } else {
                    if (state->commandText.length() >= MAX_COMMAND_LENGTH) break;
                    char ch = state->event.keyboard.unichar;
                    state->commandText += ch;
                }
            }
            else if (state->gameMode == INSIDE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                    if (state->chest.open) {
                        closeChest(state);
                    } else if (state->craftMenu.open) {
                        closeCraftingTable(state);
                    } else {
                        pauseGame(state);
                        break;
                    }
                }
            }
            else if (state->gameMode == DIALOGUE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_SPACE ||
                    state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                    al_start_timer(state->bubbleTimer);
                    resumeGame(state);
                }
            }
            else if (state->gameMode == GREENHOUSE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {  
                    pauseGame(state);
                }
            }
            else if (state->gameMode == OUTSIDE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_TAB &&
                    state->shopMenu.open) {
                    nextBuyAmount(state);
                }

                if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                    if (state->shopMenu.open) {
                        shopMenuCancel(state);
                        break;
                    }
                    pauseGame(state);
                    break;
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_SLASH) {
                    openCommandLine(state);
                    break;
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_Q) {
                    dropItem(state->handItem, state);
                    break;
                }
                if((state->event.keyboard.keycode == ALLEGRO_KEY_UP || 
                            state->event.keyboard.keycode == ALLEGRO_KEY_W) && 
                        (!state->ghost.jumping)) {
                    state->ghost.jump(state->groundLevel);
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_F) {
                    startTetris(state);
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_SLASH) {
                    openCommandLine(state);
                    break;
                }
            }
            else if (state->gameMode == CAVE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                    pauseGame(state);
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_Q) {
                    goOutside(state);
                } else if (state->event.keyboard.keycode == ALLEGRO_KEY_SLASH) {
                    openCommandLine(state);
                    break;
                }
            }
            else if (state->gameMode == PAUSED) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                    resumeGame(state);
                }
                if (state->event.keyboard.keycode == ALLEGRO_KEY_Q &&
                    state->currentMenu == &state->escMenu) {
                    quitGame(state);
                }
                break;
            }
            // tetris keys
            else if (state->gameMode == TETRIS) {
                if (state->key[ALLEGRO_KEY_LEFT] | state->key[ALLEGRO_KEY_A]) {
                    moveTetromino(state, LEFT);
                }
                else if (state->key[ALLEGRO_KEY_RIGHT] | state->key[ALLEGRO_KEY_D]) {
                    moveTetromino(state, RIGHT);
                }
                else if (state->key[ALLEGRO_KEY_DOWN] | state->key[ALLEGRO_KEY_S]) {
                    moveTetromino(state, DOWN);
                }
                else if (state->key[ALLEGRO_KEY_UP] | state->key[ALLEGRO_KEY_W]) {
                    rotateTetromino(state);
                }
                else if (state->key[ALLEGRO_KEY_SPACE])
                    fall(state);
                else if (state->event.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
                    pauseTetris(state);
                else if (state->event.keyboard.keycode == ALLEGRO_KEY_Q)
                    gameOver(state);
                else if (state->event.keyboard.keycode == ALLEGRO_KEY_SLASH) {
                    openCommandLine(state);
                    break;
                }
            }
            if (state->event.keyboard.keycode == ALLEGRO_KEY_E && 
                    state->gameMode != COMMAND && state->gameMode != PAUSED) {
                if (state->chest.open) {
                    closeChest(state);
                } else if (state->shopMenu.open) {
                    shopMenuCancel(state);
                }
                toggleInventoryOpen(state);
                break;
            }
            break;

	    case ALLEGRO_EVENT_KEY_UP:
            state->key[state->event.keyboard.keycode] &= KEY_RELEASED;
            if (state->gameMode == OUTSIDE) {
                if (state->event.keyboard.keycode == ALLEGRO_KEY_LEFT ||
                        state->event.keyboard.keycode == ALLEGRO_KEY_A) {
                    state->ghost.image = state->images.ghostNormal;
                }
                else if (state->event.keyboard.keycode == ALLEGRO_KEY_RIGHT ||
                         state->event.keyboard.keycode == ALLEGRO_KEY_D) {
                    state->ghost.image = state->images.ghostNormal;
                }
            }
            break;
        
        case ALLEGRO_EVENT_DISPLAY_CLOSE:
            state->done = true;
            break;
    }
}

// what was clicked and what to do
void whatClicked(GameState* state) {
    // book
    if (state->openBook.showing) {
        ALLEGRO_MOUSE_STATE mstate;
        al_get_mouse_state(&mstate);
        if (mstate.x > state->openBook.x &&
            mstate.x < state->openBook.x + state->openBook.width &&
            mstate.y > state->openBook.y &&
            mstate.y < state->openBook.y + state->openBook.height) {
            if (checkForMouseOnButton(&state->bookState.xButton)) {
                state->openBook.showing = false;
                state->bookState.leftPage.showing = false;
                state->bookState.rightPage.showing = false;
            }
            if (state->bookState.leftPage.hover(state)) {
                bookInteract(state, &state->bookState.leftPage);
            } else if (state->bookState.rightPage.hover(state)) {
                bookInteract(state, &state->bookState.rightPage);
            }
            return;
         }
    }
    // items
    if (state->gameMode == OUTSIDE) {
        for (StackOfItems item: state->itemsOnGround) {
            if (mouseOnGroundItem(item, state)) {
                pickUpGroundItem(state, item);
                return;
            }
        }
    }
    // ghost (hat code)
    if ((state->gameMode == OUTSIDE && state->ghost.hover(state)) ||
        (state->gameMode == INSIDE && state->insideGhost.hover(state))) {
        if (state->ghost.hat != NULL) {
            addToInventory(&state->inventory, state->ghost.hat, 1);
            state->ghost.hat = NULL;
        }
        if (state->handItem.item != NULL) {
            state->ghost.hat = state->handItem.item;
            removeFromHand(&state->handItem, 1);
        }
        return;
    }
    // customers
    for (Customer &customer: state->customers) {
        if (customer.hover(state)) {
            customer.talk(state);
        }
    }
    // other entities
    for (Entity* entity: state->toShow) {
        if (entity == NULL) continue;
        if (!entity->clickable) continue; 
        
        if (entity->hover(state)) {
            entity->click(state);
            /*
            if (entity->sound != NULL) {
                playSound(entity->sound, 0.7, state);
            }
            if (state->handItem.item != NULL && entity->feedable && entity != &state->fox) {
                entity->feed(state->handItem.item, state);
                return;
            }
            if (entity->onclick != NULL) {
                entity->onclick(state);
                return;
            }
            if (entity->hasDialogue() && entity->showing
                    && !entity->dead && entity->dialogueTimer != NULL) {
                entity->showDialogue();
            }
		    if (entity->pickupable) {
                entity->pickUp(state);
            }
            if (entity->harvestable) {
                entity->harvest(state);
            }*/
            return;
        }
    }
}

void exitGame(GameState* state) {
    state->done = true;
}
