#ifndef INVENTORY_H_INCLUDED
#define INVENTORY_H_INCLUDED

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <stdbool.h>
#include <stdio.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "structs.h"

int freeInventorySlot(Inventory* inventory);
void initInventory(Inventory* inventory, bool center, GameState* state);

void drawTooltip(Item* item, Colors* colors, Fonts* fonts);
void drawGrid(Inventory* inv);
void drawInventory(GameState* state, Inventory* inventory);

int findInInventory(Inventory* inventory, Item* item);
int addToInventory(Inventory* inventory, Item* item, int amount);
void removeFromInventory(Inventory* inventory, Item* item, int amount);

bool mouseOnInventory(Inventory* inv);
bool inventoryInteract(GameState* state, Inventory* inventory, int button);

void openInventory(GameState* state);
void closeInventory(GameState* state);
void toggleInventoryOpen(GameState* state);

void removeFromHand(StackOfItems* handItem, int amount);

#endif

