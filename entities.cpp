#include "structs.h"
#include "entities.h"
#include "levels.h"
#include "events.h"
#include "other.h"
#include "interactions.h"
#include "inventory.h"
#include <iostream>

void Entity::loadImage(string pathToImg) {
    image = al_load_bitmap(pathToImg.c_str());
    makeSize();
}

void Entity::makeSize() {
    if (image == NULL) error("Couldn't make size, image is null");
    width = bitmap_width(image);
    height = bitmap_height(image);
}

void Entity::setOnClick(void (*function)(GameState*)) {
    clickable = true;
    onclick = function;
}

bool Entity::hasDialogue() {
    return dialogue != "";
}

void Entity::makeDialogue(string text, GameState* state) {
    clickable = true;
    dialogue = text;
    initTimer(&dialogueTimer, 1.0/0.6, state);
}

void Entity::changeImage(ALLEGRO_BITMAP* newImage) {
    image = newImage;
    double groundLevel = y + height;
    makeSize();
    y = groundLevel - height;
}

void Animal::fallAsleep() {
    if (asleepImage == NULL) return;
    asleep = true;
    changeImage(asleepImage);
    dialogue = "zzzzz";  
}

void Animal::move(GameState* state) {
    if (tamed) {
        followGhost(state);
    }
    if (jumpHeight == 0) return;
    y += dy;
    if (y < state->groundLevel - height + yOffset) dy += GRAVITY;
    if (y >= state->groundLevel - height + yOffset) {
        y = state->groundLevel - height + yOffset;
        dy = 0;
    }
}

void Ghost::move(GameState* state) {
    y += dy;          
    if (y < state->groundLevel - height + yOffset) dy += GRAVITY;
    if (y >= state->groundLevel - height + yOffset) {
        y = state->groundLevel - height + yOffset;
        dy = 0;
    }
}

bool Entity::hover(GameState* state) {
    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);

    if (image != NULL) {
        width = al_get_bitmap_width(image);
        height = al_get_bitmap_height(image);
    }
    float X = xOnScreen(state->viewX);
    float Y = y + yOffset;

    return (mstate.x > X &&
            mstate.x < X + width &&
            mstate.y > Y &&
            mstate.y < Y + height);
}

void Animal::click(GameState* state) {
    if (state->handItem.item != NULL && feedable && this != &state->fox) {
        feed(state->handItem.item, state);
        return;
    }
    Entity::click(state);
}

void Entity::click(GameState* state) {
    if (sound != NULL) {
        playSound(sound, 0.7, state);
    }
    if (onclick != NULL) {
        onclick(state);
        return;
    }
    if (hasDialogue() && showing && !dead && dialogueTimer != NULL) {
        showDialogue();
    }
    if (pickupable) {
        pickUp(state);
    }
    if (harvestable) {
        harvest(state);
    }
}

Entity newSpider(GameState* state) {
    Entity spider = (Entity) {
        .type = "Spider",
        .width = (double) al_get_bitmap_width(state->images.spider),
        .height =(double) al_get_bitmap_height(state->images.spider),
        .speed = 1.9,
        .attackSpeed = 3.6,
        .health = 30,
        .maxHealth = 30,
        .damageAmount = 10,
        .image = state->images.spider,
        .normalImage = state->images.spider,
        .damagedImage = state->images.damagedSpider,
        .deadImage = state->images.deadSpider,
        .killable = true,
        .damageTimer = al_create_timer(0.4),
        .attackCooldown = al_create_timer(0.8),
        .caveSymbol = 'S',
    };
    spider.makeDialogue("grrr", state);
    return spider;
}

Entity newStrawberryPlant(GameState* state) {
    Entity plant = (Entity) {
        .type = "Strawberry plant",
        .image = state->images.strawberryPlant,
        .clickable = true,    
        .pickupable = true,
        .item = &state->strawberryItem,
        .onLevel = state->currentLevel,
    };
    plant.y = state->groundLevel - al_get_bitmap_height(plant.image);
    plant.x = rand() % state->windowWidth;
    plant.makeSize();
    return plant;
}

Entity newAppleTree(GameState* state) {
    Entity tree = (Entity) {
        .type = "Apple tree",
        .image = state->images.appleTree,
        .harvestedImage = state->images.treeImg,
        .clickable = true,
        .harvestable = true,
        .item = &state->apple,
        .onLevel = state->currentLevel,
    };
    tree.y = state->groundLevel - al_get_bitmap_height(tree.image);
    tree.x = rand() % state->windowWidth;
    tree.width = al_get_bitmap_width(tree.image);
    tree.height = 50;
    return tree;
}

void Entity::showDialogue() {
    showingDialogue = true;
    al_start_timer(dialogueTimer);
}

void makeSpiderDrop(Entity* spider, GameState* state) {
    // 1/5 chance of drop
    int n = rand() % 5;
    if (n != 0) return;

    // random gem type
    int y = rand() % GEMS;

    // NOTE: change drops to toShow?
    // find free spot in drops array
    int f = -1;
    for (int i = 0; i < 10; i++) {
        if (!state->drops[i].clickable) {
            f = i;
            break;
        }
    }
    if (f == -1)
        error("No space in drops array");

    // make drop
    state->drops[f] = (Entity) {
        .type = "Gem",
        .x = spider->x,
        .y = spider->y,
        .image = state->images.gems[y],
        .clickable = true,
        .pickupable = true,
        .item = &state->gemItems[y]
    };
    state->drops[f].xOffset = (state->bigCave.tileSize 
                             - al_get_bitmap_width(state->drops[f].image))/2;
    state->drops[f].yOffset = (state->bigCave.tileSize 
                             - al_get_bitmap_height(state->drops[f].image))/2;
    spider->drop = &state->drops[f];
}

Entity newBat(GameState* state) {
    Entity bat = (Entity) {
        .type = "Bat",
        .xOffset = state->bigCave.tileSize*0.5,
        .image = state->images.bat,
        .caveSymbol = 'B',
    };
    bat.makeSize();
    bat.makeDialogue("squeak", state);
    return bat;
}

int newEntityID(GameState* state) {
    return state->newestEntityID + 1;
}

bool bubbleEntityCollision(Bubble* bubble, Entity* entity) {
    if (!bubble->alive) return false;
    double corners[] = { bubble->x+1, bubble->y+1, bubble->x + 19, bubble->y+1,
                        bubble->x+1, bubble->y + 19, bubble->x + 19, bubble->y + 19 };
    FOR(4) {
        if (corners[2*i] > entity->x && corners[2*i] < entity->x+entity->width &&
            corners[2*i+1] > entity->y && corners[2*i+1] < entity->y+entity->height) {
            return true;
        }
    }
    return false;
}

bool entityEntityCollision(Entity* entity1, Entity* entity2) {
    if (entity1->ID == entity2->ID) return false;
    if (entity1 == NULL || entity2 == NULL) return false;
    if (!entity1->showing || !entity2->showing) return false;
    if ((entity1->killable && entity1->dead) || 
        (entity2->killable && entity2->dead)) return false;

    double corners[] = { entity1->x, entity1->y, 
                        entity1->x + entity1->width, entity1->y,
                        entity1->x, entity1->y + entity1->height, 
                        entity1->x + entity1->width, entity1->y + entity1->height };

    FOR(4) {
        if (corners[2*i] >= entity2->x && corners[2*i] <= entity2->x+entity2->width &&
            corners[2*i+1] >= entity2->y && corners[2*i+1] <= entity2->y+entity2->height) {
            return true;
        }
    }
    return false;
}

bool entityEntityDirCollision(Entity* entity1, Entity* entity2, enum direction dir) {
    if (entity1->ID == entity2->ID) return false;
    if (entity1 == NULL || entity2 == NULL) return false;
    if (!entity1->showing || !entity2->showing) return false;
    if ((entity1->killable && entity1->dead) || 
        (entity2->killable && entity2->dead)) return false;

    if (dir == UP) {
        if (entity2->y > entity1->y) return false;
        if (entity2->y + entity2->height < entity1->y) return false;
        if (entity1->x + entity1->width < entity2->x) return false;
        if (entity1->x > entity2->x + entity2->width) return false;

    } else if (dir == DOWN) {
        if (entity2->y < entity1->y) return false;
        if (entity1->y + entity1->height < entity2->y) return false;
        if (entity1->x + entity1->width < entity2->x) return false;
        if (entity1->x > entity2->x + entity2->width) return false;

    } else if (dir == LEFT) {
        if (entity1->x < entity2->x) return false;
        if (entity1->x > entity2->x + entity2->width) return false;
        if (entity2->y > entity1->y + entity1->height) return false;
        if (entity2->y + entity2->height < entity1->y) return false;
    
    } else if (dir == RIGHT) {
        if (entity1->x > entity2->x) return false;
        if (entity1->x + entity1->width < entity2->x) return false;
        if (entity2->y + entity2->height < entity1->y) return false;
        if (entity1->y + entity1->height < entity2->y) return false;
    }
    return true;
}

void Entity::damage(double amount, GameState* state) {
    if (dead) return;

    health -= amount;

    if (damageTimer != NULL)
        al_start_timer(damageTimer);
    
    // die
    if (health <= 0) {
        dead = true;
        if (deadImage != NULL) image = deadImage;
        showingDialogue = false;
        if (type == "Spider") makeSpiderDrop(this, state);
        if (type == "Ghost") goOutside(state);
        if (drop != NULL) {
            drop->addToLevel(state);
        }
        return;
    }

    if (damagedImage != NULL) image = damagedImage;
}

void Entity::draw(GameState* state) {
    if (!showing) return;
    
    // entity image
    drawImage(state);
    // hp bar
    if (health < maxHealth &&
        killable && !dead)
        drawHPBar(state);
    // dialogue
    double dialogueX = x + xOffset;
    if (onLevel != 0) {
        dialogueX += (onLevel - 1)*state->windowWidth - state->viewX;
    }
    if (showingDialogue)
        al_draw_text(state->fonts.font, state->colors.textColor,
                     dialogueX,
                     y + yOffset - state->fonts.fontSize - 5,
                     ALLEGRO_ALIGN_LEFT, dialogue.c_str());
    // hitbox
    if (state->showingHitboxes) {
        al_draw_text(state->fonts.fontSmall, state->colors.textColor,
                     xOnScreen(state->viewX), y + yOffset - 20, 
                     ALLEGRO_ALIGN_LEFT, type.c_str());
        drawHitbox(state);
    }
    // emotion
    double emoteX = x - state->viewX + 2;
    if (flipped) {
        emoteX = x + (onLevel-1)*state->windowWidth - state->viewX + width - 20;
    }
    if (happy) {
        al_draw_bitmap(state->images.heart, emoteX, y - 25, 0);
    } else if (angry) {
        al_draw_bitmap(state->images.cross, emoteX, y - 25, 0);
    }
}

void Entity::drawAtCoords(double x, double y, GameState* state) {
    if (!showing) return;
    
    // entity image
    // if (image == NULL) return;
    if (!showing) return;
    
    int flags = 0;
    if (flipped) flags = ALLEGRO_FLIP_HORIZONTAL;

    al_draw_bitmap(image, x + xOffset,
                   y + yOffset, flags);
    // hp bar
    if (health < maxHealth &&
        killable && !dead)
        drawHPBar(state);
    // dialogue
    double dialogueX = x + xOffset;
    if (showingDialogue)
        al_draw_text(state->fonts.font, state->colors.textColor,
                     dialogueX,
                     y + yOffset - state->fonts.fontSize - 5,
                     ALLEGRO_ALIGN_LEFT, dialogue.c_str());
    // hitbox
    if (state->showingHitboxes) {
        al_draw_text(state->fonts.fontSmall, state->colors.textColor,
                     x + xOffset, y + yOffset - 20, 
                     ALLEGRO_ALIGN_LEFT, type.c_str());
        drawHitbox(state);
    }
    // emotion
    double emoteX = x + 2;
    if (flipped) {
        emoteX = x + width - 20;
    }
    if (happy) {
        al_draw_bitmap(state->images.heart, emoteX, y - 25, 0);
    } else if (angry) {
        al_draw_bitmap(state->images.cross, emoteX, y - 25, 0);
    }
}

void Entity::drawImage(GameState* state) {
    if (image == NULL) return;
    if (!showing) return;
    
    int flags = 0;
    if (flipped) flags = ALLEGRO_FLIP_HORIZONTAL;

    al_draw_bitmap(image, xOnScreen(state->viewX),
                   y + yOffset, flags);
}

void Entity::drawHPBar(GameState* state) {
    double barWidth = 60;
    double barHeight = 12;

    double barX = x + xOffset + width/2 - barWidth/2;
    double barY = y + yOffset - 20;
    double barPadding = 2;

    al_draw_filled_rectangle(barX, barY, barX + barWidth, barY + barHeight,
                             al_map_rgba(112,84,126,0.5));
    al_draw_filled_rectangle(barX + barPadding, barY + barPadding, 
                             barX + (health/maxHealth)
                                * barWidth - barPadding, 
                             barY + barHeight - barPadding,
                             al_map_rgb(180,73,86));
}

void Entity::drawHitbox(GameState* state) {
    double X = x + xOffset - state->viewX;
    double Y = y + yOffset;
    double corners[] = { X, Y, 
                        X + width, Y,
                        X, Y + height, 
                        X + width, Y + height };
    double lineThickness = 1;
    ALLEGRO_COLOR color = al_map_rgba(200,200,190,150);
    
    al_draw_line(corners[0], corners[1], corners[2], corners[3],
                 color, lineThickness);
    al_draw_line(corners[2], corners[3], corners[6], corners[7],
                 color, lineThickness);
    al_draw_line(corners[6], corners[7], corners[4], corners[5],
                 color, lineThickness);
    al_draw_line(corners[4], corners[5], corners[0], corners[1],
                 color, lineThickness);
}

void Entity::becomeHappy() {
    angry = false;
    happy = true;
    al_start_timer(emoteTimer);
}

void Entity::becomeAngry() {
    angry = true;
    happy = false;
    al_start_timer(emoteTimer);
}

void Animal::feed(Item* food, GameState* state) {
    if (!food->isFood) {
        becomeAngry();
        return;
    }
    if (!likesAllFood && food != favoriteFood) {
        removeFromHand(&state->handItem, 1);
        becomeAngry();
        return;
    }
    becomeHappy();
    if (tameable && !tamed) {
        tamed = true;
        int place = 0;
        FOR(MAX_LEVEL_ENTITIES) {
            if (state->pets[i] == NULL) {
                place = i; break;
            }
        }
        FOR(MAX_LEVEL_ENTITIES) {
            if (state->animals[(state->currentLevel-1)][i] == this) {
                state->animals[state->currentLevel-1][i] = NULL;
            }
        }
        state->pets[place] = this;
    }
    removeFromHand(&state->handItem, 1);
}

void Entity::jump(double groundLevel) {       
    if (jumping) return;
    if (y < groundLevel - height + yOffset) {
        return;
    }
    dy -= jumpHeight;
    jumping = true;
}

void Animal::followGhost(GameState* state) {
    if (asleep) return;
    if (state->ghost.x > x + width + 10 && 
        state->ghost.x - x < state->windowWidth*1.8/3) {
        x += speed;
        flipped = true;
    } else if (state->ghost.x < x - 60 &&
        x - state->ghost.x < state->windowWidth*1.8/3) {
        x -= speed;
        flipped = false;
    }
    if (((state->ghost.x - x < 100 &&
            state->ghost.x - x > -100) || alwaysJumping) &&
            !jumping) {
        jump(state->groundLevel);
    }
}

void Entity::makeX(double xInLevel) {
    x = xInLevel + (onLevel-1)*1920;
}

double Entity::xOnScreen(double viewX) {
    if (onLevel == 0) {
        return x + xOffset;
    }
    return x - viewX + xOffset;
}

// this function works even for entities that have showing==false
void Entity::pickUp(GameState* state) {
    if (item == NULL) return;
    if (!pickedUp && pickupable) {
        if (addToInventory(&state->inventory, item, 1) == 0) return;
        playSound(state->bubblePop, 0.15, state);
        showing = false;
        pickedUp = true;
    }
}

void Entity::harvest(GameState* state) {
    if (addToInventory(&state->inventory, item, 1) == 0) return;
    playSound(state->bubblePop, 0.15, state);
    harvestable = false;
    image = harvestedImage;
}

void Entity::addToLevel(GameState* state) {
    state->toShow.push_back(this);
    if (!pickedUp) showing = true;
}

void Animal::addToLevel(GameState* state) {
    state->toShowAnimals.push_back(this);
    if (!pickedUp) showing = true;
}

void Entity::removeFromLevel() {
    showing = false;
    showingDialogue = false;
    if (emoteTimer != NULL) {
        al_stop_timer(emoteTimer);
    }
    if (dialogueTimer != NULL) {
        al_stop_timer(dialogueTimer);
    }
    happy = false;
    angry = false;
}

//void Animal::removeFromLevel() {
//    Entity::removeFromLevel();
//}
