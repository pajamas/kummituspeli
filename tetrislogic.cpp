#include "structs.h"
#include "tetrislogic.h"
#include "levels.h"
#include "positions.h"
#include "inventory.h"

// ------------------------ START GAME & GAME TICK -------------------------------

void startTetris(GameState* state) {
    state->gameMode = TETRIS;
    state->canShoot = false;

    calculatePlantPositions(state);
    state->stars1.y = 0;
    state->stars2.y = 600;

    closeInventory(state);
    state->bookState.open = false;
    state->handItem.item = NULL;

    state->tetris->done = false;
    state->tetris->score = 0;
    state->tetris->seedsFound = 0;
    state->tetris->linesCleared = 0;
    state->tetris->blocksSinceIBlock = 0;
    for (int i = 0; i < 20; i++) {
        for (int j = 0; j < 10; j++) {
            state->tetris->gridBlocks[i][j].occupied = false;
        }
    }
    for (int i = 0; i < 200; i++) {
        for (int j = 0; j < 4; j++) {
            state->tetris->tetrominos[i].blocks[j].visible = false;
        }
    }

    newPiece(state);
    al_set_timer_speed(state->tetris->tetrisTimer, 1.0 / 1.5);
    al_start_timer(state->tetris->tetrisTimer);
}

void restartTetris(GameState* state) {
    state->gameMode = TETRIS;

    state->tetris->score = 0;
    state->tetris->seedsFound = 0;
    state->tetris->linesCleared = 0;
    state->tetris->blocksSinceIBlock = 0;


    for (int i = 0; i < 20; i++) {
        for (int j = 0; j < 10; j++) {
            state->tetris->gridBlocks[i][j].occupied = false;
        }
    }
    for (int i = 0; i < 200; i++) {
        for (int j = 0; j < 4; j++) {
            state->tetris->tetrominos[i].blocks[j].visible = false;
        }
    }

    newPiece(state);

    al_set_timer_speed(state->tetris->tetrisTimer, 1.0 / 2);
    al_start_timer(state->tetris->tetrisTimer);
}

void tetrisTick(GameState* state) {
    // move current piece down
    moveTetromino(state, DOWN);
    // the extramovements variable makes it so when you move a piece
    // left or right, it doesn't move down right away
    if (state->tetris->extraMovements == 1) { state->tetris->extraMovements = 2; }
}

// ------------------------ NEW PIECES ----------------------------
int findFreeSpotInTetrominos(GameState* state) {
    for (int i = 0; i<200; i++) {
        bool free = true;
        for (int j = 0; j < 4; j++) {
            if (state->tetris->tetrominos[i].blocks[j].visible) {
                free = false;
            }
        }
        if (!free) { continue; }
        return i;
    }
    return -1;
}

ALLEGRO_BITMAP* getImageForBlock(GameState* state, int blockNum, 
                                 enum tetrominoType type) {
    int blockX = state->tetris->blockCoords[0][type][blockNum*2];
    int blockY = state->tetris->blockCoords[0][type][blockNum*2+1];
    int x = (blockX-3)*40;
    int y = (blockY*40)+2*type*40;
    int w = 40;
    int h = 40;
    
    ALLEGRO_BITMAP* image = al_create_sub_bitmap(
        state->tetrisBlocks,x,y,w,h);
    
    return image;
}

void newPiece(GameState* state) {
    int newType = rand() % 7;

    state->tetris->blocksSinceIBlock += 1;

    if (state->tetris->blocksSinceIBlock > 8) {
        newType = 0;
    }
    if (newType == 0) {
        state->tetris->blocksSinceIBlock = 0;
    }

    int spot = findFreeSpotInTetrominos(state);

    state->tetris->tetrominos[spot] = (tetromino) {
        .tetrominoID = spot,
        .moving = true,
        .orientation = 0,
        .type = static_cast<tetrominoType>(newType)
    };

    state->tetris->currentPiece = spot;
    state->tetris->currentTetromino = &state->tetris->tetrominos[spot];

    for (int i = 0; i < 4; i++) {
        tetrisBlock* block = &state->tetris->tetrominos[spot].blocks[i];
        
        block->x = state->tetris->blockCoords[0][newType][2*i];
        block->y = state->tetris->blockCoords[0][newType][2*i+1];
        block->visible = true;
        block->tetrominoID = spot;
        block->image = getImageForBlock(state,i,static_cast<tetrominoType>(newType));

        state->tetris->gridBlocks[block->y][block->x].block = &state->tetris->tetrominos[spot].blocks[i];
        state->tetris->gridBlocks[block->y][block->x].occupied = true;
    }
    makeGhostBlockPos(state);
}

// ------------------- MOVEMENT & COLLISION ---------------------------

bool moveTetromino(GameState* state, enum direction dir) {
    for (int i = 0; i < 4; i++) {
        if (checkCollision(state, i, dir, false)) {
            if (dir == DOWN && state->tetris->extraMovements != 1) {
                if (!checkForGameOver(state)) {
                    checkForFilledLines(state);
                    newPiece(state);
                }
                else { gameOver(state); }
            }
            return false;
        }
    }
    for (int i = 0; i < 4; i++) {
        tetrisBlock block = state->tetris->currentTetromino->blocks[i];
        state->tetris->gridBlocks[block.y][block.x].occupied = false;
    }
    switch (dir) {
        case DOWN:
            for (int i = 0; i < 4; i++) {
                state->tetris->currentTetromino->blocks[i].y += 1;
                tetrisBlock block = state->tetris->currentTetromino->blocks[i];
                state->tetris->gridBlocks[block.y][block.x].block = &state->tetris->currentTetromino->blocks[i];
                state->tetris->gridBlocks[block.y][block.x].occupied = true;
                state->tetris->extraMovements = 0;
            }
            break;
        case LEFT:
            for (int i = 0; i < 4; i++) {
                state->tetris->currentTetromino->blocks[i].x -= 1;
                tetrisBlock block = state->tetris->currentTetromino->blocks[i];
                state->tetris->gridBlocks[block.y][block.x].block = &state->tetris->currentTetromino->blocks[i];
                state->tetris->gridBlocks[block.y][block.x].occupied = true;
            }
            if (state->tetris->extraMovements == 0) { state->tetris->extraMovements = 1; }
            break;
        case RIGHT:
            for (int i = 0; i < 4; i++) {
                state->tetris->currentTetromino->blocks[i].x += 1;
                tetrisBlock block = state->tetris->currentTetromino->blocks[i];
                state->tetris->gridBlocks[block.y][block.x].block = &state->tetris->currentTetromino->blocks[i];
                state->tetris->gridBlocks[block.y][block.x].occupied = true;
            }
            if (state->tetris->extraMovements == 0) { state->tetris->extraMovements = 1; }
            break;
        default:
            break;
    }
    makeGhostBlockPos(state);
    return true;

}

void rotateTetromino(GameState* state) {
    // check collisions
    //printf("checking collisions...\n");
    for (int i = 0; i < 4; i++) {
        if (checkCollision(state, i, ROTATE, false)) {
            return;
        }
        //printf("no collision\n");
    }
    int orientation = state->tetris->currentTetromino->orientation % 4;
    int type = state->tetris->currentTetromino->type;

    // remove old positions from grid
    for (int i = 0; i < 4; i++) {
        tetrisBlock block = state->tetris->currentTetromino->blocks[i];
        state->tetris->gridBlocks[block.y][block.x].occupied = false;
    }

    // calculate new positions
    for (int i = 0; i < 4; i++) {
        tetrisBlock* block = &state->tetris->currentTetromino->blocks[i];
        int xNow = state->tetris->blockCoords[orientation][type][2*i];
        int yNow = state->tetris->blockCoords[orientation][type][2*i + 1];
        int xNew = state->tetris->blockCoords[(orientation+1)%4][type][2*i];
        int yNew = state->tetris->blockCoords[(orientation+1)%4][type][2*i + 1];

        block->x += xNew-xNow;
        block->y += yNew-yNow;
    }

    // add new positions to grid
    for (int i = 0; i < 4; i++) {
        tetrisBlock block = state->tetris->currentTetromino->blocks[i];
        state->tetris->gridBlocks[block.y][block.x].occupied = true;
        state->tetris->gridBlocks[block.y][block.x].block = &state->tetris->currentTetromino->blocks[i];

    }
    state->tetris->currentTetromino->orientation += 1;
    makeGhostBlockPos(state);
}

bool checkCollision(GameState* state, int blockNum, 
                    enum direction dir, bool ghostBlock) {
    if (ghostBlock) {
        tetrisBlock block = state->tetris->ghostBlock.blocks[blockNum];
        if (block.y == state->tetris->gridHeight-1) {
            return true;
        }
        if (state->tetris->gridBlocks[block.y+1][block.x].occupied && 
            state->tetris->gridBlocks[block.y+1][block.x].block->tetrominoID != block.tetrominoID) {
            return true;
        }
        return false;
    }
    int orientation, type, xOld, yOld, xNew, yNew;
    int xAdd = 0;
    int yAdd = 0;
    tetrisBlock block = state->tetris->currentTetromino->blocks[blockNum];
    switch (dir) {
        case DOWN:
            if (block.y == state->tetris->gridHeight-1) {
                return true;
            }
            yAdd = 1;
            break;
        case LEFT:
            if (block.x == 0) {
                return true;
            }
            xAdd = -1;
            break;
        case RIGHT:
            if (block.x == state->tetris->gridWidth-1) {
                return true;
            }
            xAdd = 1;
            break;
        case ROTATE:
            orientation = state->tetris->currentTetromino->orientation % 4;
            type = state->tetris->currentTetromino->type;

            xOld = state->tetris->blockCoords[orientation][type][2*blockNum];
            yOld = state->tetris->blockCoords[orientation][type][2*blockNum + 1];
            xNew = state->tetris->blockCoords[(orientation+1)%4][type][2*blockNum];
            yNew = state->tetris->blockCoords[(orientation+1)%4][type][2*blockNum + 1];

            xAdd = xNew - xOld;
            yAdd = yNew - yOld;

            if ( block.x + xAdd < 0 || block.x + xAdd >= state->tetris->gridWidth ) {
                return true;
            }
            if ( block.y + yAdd < 0 || block.y + yAdd >= state->tetris->gridHeight ) {
                return true;
            }
        default:
            break;
    }
    if (state->tetris->gridBlocks[block.y + yAdd][block.x + xAdd].occupied && 
        state->tetris->gridBlocks[block.y + yAdd][block.x + xAdd].block->tetrominoID 
            != block.tetrominoID) {
        return true;
    }
    return false;
}

void fall(GameState* state) {
    while (true) {
        if (!moveTetromino(state, DOWN))
            break;
    }
}

// ---------------------- GHOST BLOCK ----------------------------------

void makeGhostBlockPos(GameState* state) {
    state->tetris->ghostBlock = state->tetris->tetrominos[state->tetris->currentPiece];
    while (true) {
        for (int i = 0; i < 4; i++) {
            if (checkCollision(state,i,DOWN,true)) {
                return;
            }
        }
        for (int i = 0; i < 4; i++) {
            state->tetris->ghostBlock.blocks[i].y += 1;
        }
    }
}

// -------------------------- CLEARING LINES -------------------------

void checkForFilledLines(GameState* state) {
    int filledNum = 0;
    for (int i = 0; i < state->tetris->gridHeight; i++) {
        bool filled = true;
        for (int j = 0; j < state->tetris->gridWidth; j++) {
            if (!state->tetris->gridBlocks[i][j].occupied) {
                filled = false;
            }
        }
        if (filled) {
            deleteFilledLine(state, i);
            filledNum++;
        }
    }
    state->tetris->linesCleared += filledNum;
    int scores[] = { 0, 100, 300, 500, 800 };
    state->tetris->score += scores[filledNum];
    
    int random = rand() % 100;
    int lims[] = { 0, 10, 25, 50, 100 };
    if (random < lims[filledNum]) {
        state->tetris->seedsFound += 1;
        addToInventory(&state->inventory, &state->seedItem, 1);
    }
}

void deleteFilledLine(GameState* state, int lineNum) {
    int gridWidth = state->tetris->gridWidth;

    // make the deleted blocks invisible and clear them in grid
    for (int i = 0; i < gridWidth; i++) {
        tetrisBlock* block = state->tetris->gridBlocks[lineNum][i].block;

        if (block->y == lineNum) {
            block->visible = false;
            state->tetris->gridBlocks[lineNum][i].occupied = false;
        }
    }
    // move blocks above deleted line down
    for (int i = lineNum-1; i >= 0; i--) {                                  
        for (int j = 0; j < state->tetris->gridWidth; j++) {
            if (!state->tetris->gridBlocks[i][j].occupied) {
                continue;
            }
            tetrisBlock* block = state->tetris->gridBlocks[i][j].block;

            if (block->y == i && block->visible) { 
                
                state->tetris->gridBlocks[i][j].occupied = false;
                
                block->y += 1;
                state->tetris->gridBlocks[i+1][j].block = block;
                state->tetris->gridBlocks[i+1][j].occupied = true;

            }   
        }
    }

    al_set_timer_speed(state->tetris->tetrisTimer, al_get_timer_speed(state->tetris->tetrisTimer) * 0.991);
}

// -------------------- PAUSING & END OF GAME -----------------------------

void pauseTetris(GameState* state) {
    state->previousGameMode = TETRIS;
    state->gameMode = PAUSED;
    al_stop_timer(state->tetris->tetrisTimer);
    state->currentMenu = &state->tetris->escMenu;
}

bool checkForGameOver(GameState* state) {
    for (int i = 0; i < 4; i++) {
        if (state->tetris->currentTetromino->blocks[i].y < 1) {
            return true;
        }
    }
    return false;
}

void gameOver(GameState* state) {
    state->previousGameMode = TETRIS;
    state->gameMode = PAUSED;
    al_stop_timer(state->tetris->tetrisTimer);
    state->currentMenu = &state->tetris->endMenu;
}

void endTetris(GameState* state) {
    goOutside(state);
}

// --------------------------------------------------------------------
