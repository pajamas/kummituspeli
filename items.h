#ifndef ITEMS_H_INCLUDED
#define ITEMS_H_INCLUDED

bool mouseOnGroundItem(StackOfItems item, GameState* state);
void pickUpGroundItem(GameState* state, StackOfItems item);
void drawItemOnGround(StackOfItems item, GameState* state);
void dropItem(StackOfItems item, GameState* state);
void calcItemPositions(GameState* state);

#endif
