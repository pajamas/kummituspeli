#ifndef POSITIONS_H_INCLUDED
#define POSITIONS_H_INCLUDED

void calculatePlantPositions(GameState* state);
void calcBeePos(GameState* state);
void calcSeedPos(GameState* state);
void calcBatPos(Cave* cave, int batNum);
void calcSpiderPos(Cave* cave, int spiderNum);
void calcSpiderMove(Entity* spider, Entity* ghost, Cave* cave, GameState* state);

#endif
