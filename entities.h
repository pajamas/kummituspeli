#ifndef ENTITIES_H_INCLUDED
#define ENTITIES_H_INCLUDED

Entity newSpider(GameState* state);
Entity newStrawberryPlant(GameState* state);
Entity newAppleTree(GameState* state);
Entity newBat(GameState* state);
void makeSpiderDrop(Entity* spider, GameState* state);
int newEntityID(GameState* state);

bool bubbleEntityCollision(Bubble* bubble, Entity* entity);
bool entityEntityCollision(Entity* entity1, Entity* entity2);
bool entityEntityDirCollision(Entity* entity1, Entity* entity2, enum direction dir);

#endif
