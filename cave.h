#ifndef CAVE_H_INCLUDED
#define CAVE_H_INCLUDED

#include "structs.h"

void drawCaveGamemode(GameState* state);
bool checkForBlockInBetween(Cave* cave, int x, int y, int ghostX, int ghostY);
void goIntoCave(GameState* state);
bool caveCollisionCheck(Cave* cave, Entity* entity, enum direction dir);
int freeCavePosition(string shape);
int randomCavePosition(string shape);

int xToCaveX(Cave* cave, float x);
int yToCaveY(Cave* cave, float y);

void addMineralToCave(GameState* state, Cave* cave, enum mineralTypes mineralType);
void addEntityToCave(Cave* cave, Entity* entity);
void makeSpiders(GameState* state);

void leaveCave(GameState* state);

void bubbleCaveCollisions(GameState* state, Bubble* bubble);

#endif
