#ifndef CHEST_H_INCLUDED
#define CHEST_H_INCLUDED

void initChest(GameState* state);
void openChest(GameState* state);
void closeChest(GameState* state);
void drawChestUI(GameState* state);

#endif
