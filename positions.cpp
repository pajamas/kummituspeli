#include "structs.h"
#include "cave.h"
#include "entities.h"

void calcBeePos(GameState* state) {    
    state->bee.pos = state->bee.pos - 0.025;
    state->bee.xOffset = (cos(state->bee.pos)*50)/2;
    state->bee.yOffset = sin(state->bee.pos)*50;
}

void calcSeedPos(GameState* state) {
    state->seed.pos = state->seed.pos + 0.035;
    state->seed.yOffset = sin(state->seed.pos)*7;
}

void calcSpiderPos(Cave* cave, int spiderNum) {
    int Ss = 0;
    for (int i = 0; i < cave->height; i++) {
        for (int j = 0; j < cave->width; j++) {
            if (cave->shape[i*cave->width + j] == 'S') {
                if (spiderNum == Ss) {
                    cave->spiders[spiderNum].x = cave->x + j*cave->tileSize;
                    cave->spiders[spiderNum].y = cave->y + i*cave->tileSize + 10;
                    return;
                } else {
                    Ss += 1;
                }
            }
        }
    }
}

void calcSpiderMove(Entity* spider, Entity* ghost, Cave* cave, 
                    GameState* state) {
    if (spider == NULL || spider->dead) return;
    if (!checkForBlockInBetween(cave,xToCaveX(cave,spider->x),
                                yToCaveY(cave,spider->y),
                                ghost->x+ghost->width/2,
                                ghost->y+ghost->height/2)) {
        float spiderSpeed = spider->speed;
        if (spider->attacking)
            spiderSpeed = spider->attackSpeed;
        if (spider->attackCooldown != NULL && 
            al_get_timer_started(spider->attackCooldown))
            spiderSpeed = -0.8;

        Vector2d diff = newVector2d(ghost->x+ghost->width/2-spider->x,
                                    ghost->y+ghost->height/2-spider->y);
        if (vector2dLength(diff) > 250) return;
        if (vector2dLength(diff) < 70) {
            spider->attacking = true;
        } else {
            spider->attacking = false;
        }
        if (vector2dLength(diff) < 20 &&
            !al_get_timer_started(spider->attackCooldown)) {
            state->ghost.damage(spider->damageAmount, state);
            if (spider->attackCooldown != NULL)
                al_start_timer(spider->attackCooldown);
            spider->attacking = false;
        }
        diff = unitVector2d(diff);
        diff = vector2dMultiply(diff,spiderSpeed);
        enum direction dirX, dirY;
        if (diff.x > 0) {
            dirX = RIGHT;
        } else {
            dirX = LEFT;
        }
        if (diff.y > 0) {
            dirY = DOWN;
        } else {
            dirY = UP;
        }
        bool collisionX = false;
        bool collisionY = false;
        
        FOR(MAX_SPIDERS) {
            if (entityEntityDirCollision(spider,&cave->spiders[i],dirX)) {
                collisionX = true;
            }
            if (entityEntityDirCollision(spider,&cave->spiders[i],dirY)) {
                collisionY = true;
            }
        }

        if (!caveCollisionCheck(cave, spider, dirX) && !collisionX) {
            spider->x += diff.x;
        }
        if (!caveCollisionCheck(cave, spider, dirY) && !collisionY) {
            spider->y += diff.y;
        }
    } else {
        return;
    }
}

// make random positions for all the trees and plants
void calculatePlantPositions(GameState* state) {
    int interval;
    int mod;
    if (state->gameMode == TETRIS) {
        interval = 42;
        mod = 50;
    }
    else {
        interval = 20;
        mod = 105;
    }

    float seedX = state->seed.x;
    for (Tree &t: state->trees) {
        t.x = interval*(rand() % mod);
        while(state->currentLevel==1 && t.x<seedX + 100 && t.x > seedX - 80) {  
            t.x = interval*(rand() % mod);
        }
	    t.img = state->images.treeImg;
        t.height = al_get_bitmap_height(t.img);
    }
    for (Plant &p: state->plants) {
	    p.img = state->plantspng[(rand() % PLANT_SPECIES_NUM)];
        while(state->currentLevel==1 && p.x<seedX + 60 && p.x > seedX - 80) {  
            p.x = rand() % 2100;
        }
        p.y = state->groundLevel - al_get_bitmap_height(p.img) + (rand() % 3);
    }
}

