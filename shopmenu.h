#ifndef SELLMENU_H_DEFINED
#define SELLMENU_H_DEFINED

void initShopMenu(GameState* state);
void drawShopMenu(GameState* state);
bool shopMenuInteract(GameState* state, int button);
void shopMenuOk(GameState* state);
void nextBuyAmount(GameState* state);
void openShopMenu(GameState* state);
void closeShopMenu(GameState* state);
void shopMenuCancel(GameState* state);
void shopMenuTrash(GameState* state);

#endif
