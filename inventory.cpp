#include "structs.h"
#include "events.h"
#include "draw.h"
#include "inventory.h"
#include "entities.h"

int freeInventorySlot(Inventory* inventory) {
    int freeSlot = -1;
    FOR(inventory->size) {
        if (inventory->slots[i].itemsContained.item == NULL) {
            freeSlot = i;
            break;
        }
    }
    return freeSlot;
}

void initInventory(Inventory* inventory, bool center, GameState* state) {
    int padding = inventory->padding;
    int slotWidth = inventory->slotWidth;

    if (center) {
        inventory->x = state->windowWidth/2 - inventory->widthPx/2;
        inventory->y = state->windowHeight/2 - inventory->heightPx/2;
    }

    float x = inventory->x;
    float y = inventory->y;

    int slot;
    FORI(inventory->height) {
        FORJ(inventory->width) {
            slot = i*inventory->width + j;
            inventory->slots[slot].x = x + padding + j*(slotWidth + padding);
            inventory->slots[slot].y = y + padding + i*(slotWidth + padding);
            inventory->slots[slot].width = slotWidth;
            inventory->slots[slot].height = slotWidth;
        }
    }
}

void drawTooltip(Item* item, GameState* state) {
    if (item == NULL) return;

    const char* text = item->name.c_str();

    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);

    float x = mstate.x;
    float y = mstate.y - INVENTORY_SLOT_WIDTH;
    float x2 = x + 20 + al_get_text_width(state->fonts.fontMedium, text);
    float y2 = y + 20 + state->fonts.fontSizeMedium;

    al_draw_filled_rectangle(x, y, x2, y2, state->colors.menuBgColorTransparent);

    al_draw_text(state->fonts.fontMedium, state->colors.textColor, x + 10, y + 10, 
                 ALLEGRO_ALIGN_LEFT, text);
}

void drawTooltipWithBuyPrice(Item* item, GameState* state) {
    if (item == NULL) return;

    const char* text = item->name.c_str();

    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);

    float x = mstate.x;
    float y = mstate.y - INVENTORY_SLOT_WIDTH*1.5;
    float x2 = x + 20 + al_get_text_width(state->fonts.fontMedium, text);
    float y2 = y + 30 + 2*state->fonts.fontSizeMedium;

    al_draw_filled_rectangle(x, y, x2, y2, state->colors.menuBgColorTransparent);

    al_draw_text(state->fonts.fontMedium, state->colors.textColor, x + 10, y + 10, 
                 ALLEGRO_ALIGN_LEFT, text);

    al_draw_bitmap(state->coin.image, x + 10, y + 32, 0);
    al_draw_text(state->fonts.fontMedium, state->colors.textColor, x + 45, y + 40, 
                 ALLEGRO_ALIGN_LEFT, to_string(item->buyPrice).c_str());
}

void drawGrid(Inventory* inv) {
    al_draw_filled_rectangle(inv->x, inv->y,
                            inv->x + (inv->width+1)*inv->padding + inv->width*inv->slotWidth,
                            inv->y + (inv->height+1)*inv->padding + inv->height*inv->slotWidth,
                            inv->bgColor);
    
    FOR(inv->size) {
        al_draw_filled_rectangle(inv->slots[i].x, inv->slots[i].y,
                                 inv->slots[i].x + inv->slotWidth,
                                 inv->slots[i].y + inv->slotWidth,
                                 inv->slotBgColor);
    }
}

int hoverSlotNum(Inventory* inventory) {
    int num = -1;
    ALLEGRO_MOUSE_STATE mouse;
    al_get_mouse_state(&mouse);
    if (mouse.x > inventory->x &&
        mouse.x < inventory->x + inventory->widthPx - inventory->padding &&
        mouse.y > inventory->y &&
        mouse.y < inventory->y + inventory->heightPx - inventory->padding) {
        int slotSize = inventory->padding + inventory->slotWidth;
        double mouseXInGrid = mouse.x - inventory->x;
        double mouseYInGrid = mouse.y - inventory->y;
        int slotX = floor(mouseXInGrid/slotSize);            
        int slotY = floor(mouseYInGrid/slotSize);            
        int slotI = slotY*inventory->width + slotX;
        if (mouseXInGrid > slotX*slotSize + inventory->padding && 
            mouseYInGrid > slotY*slotSize + inventory->padding) {
            num = slotI;
        }
    }
    return num;
}

void drawInventory(GameState* state, Inventory* inventory) {
    drawGrid(inventory);

    int slotI = hoverSlotNum(inventory);

    // hover highlight
    if (slotI != -1) {
        int padding = (inventory->slotWidth - al_get_bitmap_width(state->images.inventoryHover))/2;
        al_draw_rectangle(inventory->slots[slotI].x + padding, 
                          inventory->slots[slotI].y + padding,
                          inventory->slots[slotI].x + inventory->slotWidth - padding,
                          inventory->slots[slotI].y + inventory->slotWidth - padding,
                          al_map_rgba(210,155,220,200), 6);
    }

    FOR(inventory->size) {
        // draw item
        if (inventory->slots[i].itemsContained.item != NULL) {
            ALLEGRO_BITMAP* img = inventory->slots[i].itemsContained.item->image;
            float imgX = inventory->slots[i].x
                         + inventory->slotWidth / 2 
                         - al_get_bitmap_width(img) / 2;
            float imgY = inventory->slots[i].y
                         + inventory->slotWidth / 2 
                         - al_get_bitmap_height(img) / 2;
            al_draw_bitmap(inventory->slots[i].itemsContained.item->image, 
                           imgX, imgY, 0);
            // amount
            if (inventory->slots[i].itemsContained.amount > 1) {
                string amountText = to_string(inventory->slots[i].itemsContained.amount);
                float textX = inventory->slots[i].x + inventory->slotWidth;
                float textY = inventory->slots[i].y + inventory->slotWidth - 20;
                al_draw_text(state->fonts.fontSmall, state->colors.textColor, textX, textY,
                             ALLEGRO_ALIGN_RIGHT, amountText.c_str());
            }
        }
    }

    // draw tooltip
    void (*drawTooltipFunction)(Item*, GameState*) = drawTooltip;
    if (inventory == &state->shopMenu.shopItems) {
        drawTooltipFunction = drawTooltipWithBuyPrice;
    }

    if (slotI != -1) {
        drawTooltipFunction(inventory->slots[slotI].itemsContained.item, state);
    }
}

int findInInventory(Inventory* inventory, Item* item) {
    FOR(inventory->size) {
        if (inventory->slots[i].itemsContained.item == item) {
            if (inventory->slots[i].itemsContained.amount == 999) continue;
            return i;
        }
    }
    return -1;
}

// returns amount added
int addToInventory(Inventory* inventory, Item* item, int amount) {
    if (amount <= 0) return 0;
    int added = 0;

    // if large number of items being added
    if (amount > 999) {
        while (amount > 0) {
            int toAdd;
            if (amount > 999) { toAdd = 999; }
            else { toAdd = amount; }
            added += addToInventory(inventory, item, toAdd);
            amount -= toAdd;
        }
        return added;
    }
    int slot = findInInventory(inventory, item);
    // if none in inventory yet
    if (slot == -1) {
        slot = freeInventorySlot(inventory);
        // inventory full
        if (slot == -1) {
            return 0;
        }
        inventory->slots[slot].itemsContained.item = item;
        inventory->slots[slot].itemsContained.amount = amount;
        return amount;
    // if item in inventory already
    } else {
        // if cannot fit all items into one slot
        if (inventory->slots[slot].itemsContained.amount + amount > 999) {
            int addable = 999 - inventory->slots[slot].itemsContained.amount;
            inventory->slots[slot].itemsContained.amount += addable;
            int otherSlot = freeInventorySlot(inventory);
            if (otherSlot == -1) { return addable; }
            inventory->slots[otherSlot].itemsContained.item = item;
            inventory->slots[otherSlot].itemsContained.amount = amount - addable;
            return amount;
        // if can fit all items into one slot
        } else {
            inventory->slots[slot].itemsContained.amount += amount;
            return amount;
        }
    }
}

// add to non-empty slot
int addToInventorySlot(Inventory* inventory, int slot, int amount) {
    if (amount <= 0) return 0;
    if (inventory->slots[slot].itemsContained.amount + amount > 999) {
        int addable = 999 - inventory->slots[slot].itemsContained.amount;
        inventory->slots[slot].itemsContained.amount += addable;
        return addable;
    } else {
        inventory->slots[slot].itemsContained.amount += amount;
        return amount;
    }
}

// add to empty slot
void addItemsToInventorySlot(Inventory* inventory, int slot, Item* item, int amount) {
    if (amount <= 0) return;
    inventory->slots[slot].itemsContained.item = item;
    inventory->slots[slot].itemsContained.amount = amount;
}

void removeFromInventory(Inventory* inventory, Item* item, int amount) {
    if (amount <= 0) return;
    int slot = findInInventory(inventory, item);
    inventory->slots[slot].itemsContained.amount -= amount;
    if (inventory->slots[slot].itemsContained.amount == 0) {
        inventory->slots[slot].itemsContained.item = NULL;
    }
}

void removeFromInventorySlot(Inventory* inventory, int slot, int amount) {
    if (amount <= 0) return;
    inventory->slots[slot].itemsContained.amount -= amount; 
    if (inventory->slots[slot].itemsContained.amount == 0) {
        inventory->slots[slot].itemsContained.item = NULL;
    }
}

bool mouseOnInventory(Inventory* inv) {
    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);
                        
    float width = inv->widthPx;
    float height = inv->heightPx;
    float x = inv->x;
    float y = inv->y;
                        
    return (mstate.x > x &&   
            mstate.x < x + width &&
            mstate.y > y &&
            mstate.y < y + height);
}

bool inventoryInteract(GameState* state, Inventory* inventory, int button) {
    if (!mouseOnInventory(inventory)) {
        return false;
    }

    if ((inventory == &state->shopMenu.buyInventory || 
         inventory == &state->shopMenu.shopItems) &&
        state->handItem.item != NULL && !state->handItem.notOwned) {
        return true;
    }

    if (inventory == &state->shopMenu.shopItems &&
        state->handItem.item != NULL) {
        if (state->handItem.notOwned) {
            removeFromHand(&state->handItem, state->handItem.amount);
        }
        return true;
    }

    if (inventory == &state->shopMenu.sellInventory &&
        state->handItem.item != NULL &&
        (state->handItem.item->sellPrice == -1 || state->handItem.notOwned)) {
        return true;
    }

    if (inventory == &state->inventory && state->handItem.item != NULL &&
        state->handItem.notOwned) {
        return true;
    }

    if (button != 1 && button != 2) return true;
    int amount = 1;

    Inventory* inv = inventory;

    int i = hoverSlotNum(inv);
    if (i == -1) return true;

    if (button == 1)
        amount = inv->slots[i].itemsContained.amount;
    if (state->handItem.item == NULL && inv->slots[i].itemsContained.item == NULL) return true;
    if (inventory == &state->shopMenu.shopItems) {
        state->handItem.item = inv->slots[i].itemsContained.item;
        state->handItem.amount = amount;
        state->handItem.notOwned = inv->slots[i].itemsContained.notOwned;
        return true;
    }
    // empty hand over item
    if (state->handItem.item == NULL && inv->slots[i].itemsContained.item != NULL) {
        // shift click + chest open
        if (state->key[ALLEGRO_KEY_LSHIFT] && state->chest.open) {
            if (inv == &state->inventory) {
                int added = addToInventory(&state->chest, inv->slots[i].itemsContained.item, inv->slots[i].itemsContained.amount);
                removeFromInventorySlot(inventory, i, added);
            } else {
                int added = addToInventory(&state->inventory, 
                               inv->slots[i].itemsContained.item, inv->slots[i].itemsContained.amount);
                removeFromInventorySlot(inv, i, added);
            }
            return true;
        }
        // item onclick
        if (inventory == &state->inventory && !state->chest.open && 
            inv->slots[i].itemsContained.item->onclick != NULL && !state->shopMenu.open
            && button == 1) {
            inv->slots[i].itemsContained.item->onclick(state);
            return true;
        }
        // take into hand
        state->handItem.item = inv->slots[i].itemsContained.item;
        state->handItem.amount = amount;
        state->handItem.notOwned = inv->slots[i].itemsContained.notOwned;
        removeFromInventorySlot(inv, i, amount);
        return true;
    } else if (state->handItem.item != NULL && inv->slots[i].itemsContained.item != NULL &&
               state->handItem.item == inventory->slots[i].itemsContained.item) {
        if (state->handItem.item->nonStackable) return true;
        // put items in inventory slot when same type of items already in slot
        if (button == 1) {
            amount = state->handItem.amount;
            int added = addToInventorySlot(inventory, i, amount);
            removeFromHand(&state->handItem, added);
        // take items into hand when items already in hand
        } else if (button == 2 && state->handItem.notOwned == inventory->slots[i].itemsContained.notOwned) {
            state->handItem.amount += amount;
            removeFromInventorySlot(inventory, i, amount);
        }
        return true;
    // switch items
    } else if (state->handItem.item != NULL && inventory->slots[i].itemsContained.item != NULL &&
               state->handItem.item != inventory->slots[i].itemsContained.item) {
        StackOfItems wasInHand = state->handItem;
        state->handItem = inventory->slots[i].itemsContained;
        inventory->slots[i].itemsContained = wasInHand;
        return true;
    // add to empty inventory slot
    } else {
        if (button == 1) {
            addItemsToInventorySlot(inventory, i, 
                                    state->handItem.item, state->handItem.amount);
            state->handItem.item = NULL;
            return true;
        } else if (button == 2) {
            addItemsToInventorySlot(inventory, i, state->handItem.item, 1);
            removeFromHand(&state->handItem, 1);
            return true;
        }
    }
    return true;
}

void openInventory(GameState* state) {
    state->inventory.open = true;
}

void closeInventory(GameState* state) {
    state->inventory.open = false;
}

void toggleInventoryOpen(GameState* state) {
    state->inventory.open = !state->inventory.open;
}

void Inventory::loadItems(GameState* state) {
    for (InventorySlot& slot : slots) {
        StackOfItems* stack = &slot.itemsContained; 
        if (stack->itemID != -1) {
            stack->item = state->allItems[stack->itemID];
        }
    }
}
