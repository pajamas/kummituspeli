#ifndef COMMAND_H_INCLUDED
#define COMMAND_H_INCLUDED

void openCommandLine(GameState* state);
void closeCommandLine(GameState* state);
void drawCommandLine(GameState* state);
void parseCommand(GameState* state);

#endif
