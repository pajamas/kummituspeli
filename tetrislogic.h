#ifndef TETRISLOGIC_H_INCLUDED
#define TETRISLOGIC_H_INCLUDED

#include "structs.h"

enum tetrominoType { I,O,S,Z,T,J,L };

typedef struct tetrisBlock {
    bool visible;
    ALLEGRO_BITMAP* image;
    int x,y;
    int tetrominoID;
} tetrisBlock;

typedef struct tetromino {
    int tetrominoID;
    bool moving;
    int orientation;
    tetrisBlock blocks[4];
    enum tetrominoType type;
} tetromino;

typedef struct gridBlock {
    bool occupied;
    tetrisBlock* block;
} gridBlock;

struct tetrisState {
    bool done;
    int gridHeight, gridWidth;
    int blockHeight;
    int score, seedsFound, linesCleared;
    int currentPiece;
    tetromino* currentTetromino;
    tetromino ghostBlock;
    ALLEGRO_TIMER* tetrisTimer;
    gridBlock gridBlocks[20][10];
    tetromino tetrominos[200];
    const int blockCoords[4][7][8] = {{
        { 3,0, 4,0, 5,0, 6,0 }, // I
        { 4,0, 5,0, 4,1, 5,1 }, // O
        { 3,1, 4,1, 4,0, 5,0 }, // S
        { 3,0, 4,1, 4,0, 5,1 }, // Z
        { 3,0, 4,0, 5,0, 4,1 }, // T
        { 3,0, 4,0, 5,0, 5,1 }, // J
        { 3,0, 4,0, 5,0, 3,1 } // L
        },{
        { 4,-1, 4,0, 4,1, 4,2 }, // I
        { 5,0, 5,1, 4,0, 4,1 }, // O
        { 3,-1, 3,0, 4,0, 4,1 }, // S
        { 5,-1, 4,0, 5,0, 4,1 }, // Z
        { 4,-1, 4,0, 4,1, 3,0 }, // T
        { 5,-1, 5,0, 5,1, 4,1 }, // J
        { 4,0, 4,1, 4,2, 3,0 } // L
        },{
        { 6,-1, 5,-1, 4,-1, 3,-1 }, // I
        { 5,1, 4,1, 5,0, 4,0 }, // O
        { 5,0, 4,0, 4,1, 3,1 }, // S
        { 5,1, 4,0, 4,1, 3,0 }, // Z
        { 5,0, 4,0, 3,0, 4,-1 }, // T
        { 6,1, 5,1, 4,1, 4,0 }, // J
        { 4,1, 3,1, 2,1, 4,0 } // L
        },{
        { 4,2, 4,1, 4,0, 4,-1 }, // I
        { 4,1, 4,0, 5,1, 5,0 }, // O
        { 4,1, 4,0, 3,0, 3,-1 }, // S
        { 4,1, 5,0, 4,0, 5,-1 }, // Z
        { 4,1, 4,0, 4,-1, 5,0 }, // T
        { 4,2, 4,1, 4,0, 5,0 }, // J
        { 3,1, 3,0, 3,-1, 4,1 } // L
    }};
    int extraMovements;
    int blocksSinceIBlock;

    Button endOkButton;
    Menu endMenu;
    Menu escMenu;
};

// START GAME & GAME TICK
void startTetris(GameState* state);
void tetrisTick(GameState* state);
// NEW PIECES
int findFreeSpotInTetrominos(GameState* state);
ALLEGRO_BITMAP* getImageForBlock(GameState* state, int blockNum, 
                                 enum tetrominoType type);
void newPiece(GameState* state);
// MOVEMENT & COLLISION
bool moveTetromino(GameState* state, enum direction dir);
void rotateTetromino(GameState* state);
bool checkCollision(GameState* state, int blockNum, enum direction dir, bool ghostBlock);
void fall(GameState* state);
// GHOST BLOCK
void makeGhostBlockPos(GameState* state);
// CLEARING LINES
void checkForFilledLines(GameState* state);
void deleteFilledLine(GameState* state, int lineNum);
// PAUSING & END OF GAME
void pauseTetris(GameState* state);
void restartTetris(GameState* state);
bool checkForGameOver(GameState* state);
void gameOver(GameState* state);
void endTetris(GameState* state);

#endif
