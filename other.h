#ifndef OTHER_H_INCLUDED
#define OTHER_H_INCLUDED

bool strEquals(char* a, char* b);
void error(string message);
void soundToggle(GameState* state);
void playSound(ALLEGRO_SAMPLE* sound, float volume, GameState* state);
void initTimer(ALLEGRO_TIMER** timer, double t, GameState* state);

#endif
