#include "structs.h"
#include "positions.h"
#include "draw.h"
#include "events.h"
#include "chest.h"
#include "other.h"
#include "entities.h"

void clearLevel(GameState* state) {
    state->shopMenu.open = false;
    state->chest.open = false;
    state->chestEntity.onclick = openChest;

    FOR(MAX_BUBBLES) {
        state->bubbles[i].alive = false;
    }
    FOR(state->toShow.size()) {
        if (state->toShow[i] == NULL) continue;
        state->toShow[i]->removeFromLevel();
    }
    state->toShow.clear();
    FOR(state->toShowAnimals.size()) {
        if (state->toShowAnimals[i] == NULL) continue;
        state->toShowAnimals[i]->removeFromLevel();
    }
    state->toShowAnimals.clear();
}

void genNextLevel(GameState* state) {
    int level = state->currentLevel;

    for (int i = state->toShow.size() - 1; i >= 0; i--) {
        if (state->toShow[i]->onLevel == level + 1) return;                            
        if (state->toShow[i]->onLevel <= level - 2) {                            
            state->toShow.erase(state->toShow.begin() + i);
        }
    }

    if (level > 4) return;
    FOR(MAX_LEVEL_ENTITIES) {
        if (state->entities[level][i] == NULL) continue;
        state->entities[level][i]->addToLevel(state);
    }
}

void genPreviousLevel(GameState* state) {
    int level = state->currentLevel;
    
    for (int i = state->toShow.size() - 1; i >= 0; i--) {
        if (state->toShow[i]->onLevel == level - 1) return;
        if (state->toShow[i]->onLevel >= level + 2) {  
            state->toShow.erase(state->toShow.begin() + i);
        }
    }
    if (level == 1) return;
    if (level - 2 > 4) return;
    FOR(MAX_LEVEL_ENTITIES) {
        if (state->entities[level-2][i] == NULL) continue;
        state->entities[(level-2)][i]->addToLevel(state);
    }
}

void genLevel(int level, GameState* state) {
    calculatePlantPositions(state);

    clearLevel(state);

    int randomNum = rand() % 10;
    if (randomNum < 4) {
        state->strawberryPlant = newStrawberryPlant(state);
        state->strawberryPlant.addToLevel(state);
    }
    int randomNum2 = rand() % 10;
    if (randomNum2 < 3) {
        state->appleTree = newAppleTree(state);
        state->appleTree.addToLevel(state);
    }

    FOR(MAX_LEVEL_ENTITIES) {
        if (state->entities[(level-1)][i] == NULL) continue;
        state->entities[(level-1)][i]->addToLevel(state);
    }
    FOR(MAX_LEVEL_ENTITIES) {
        if (state->animals[(level-1)][i] == NULL) continue;
        state->animals[(level-1)][i]->addToLevel(state);
    }
    FOR(state->pets.size()) {
        if (state->pets[i] == NULL) continue;
        state->pets[i]->x = state->ghost.x;
        state->pets[i]->addToLevel(state);
    }
    
    /*if (level > 2) {
	    state->backArrow.addToLevel(state, &state->backArrow);
    }*/
    genNextLevel(state);
    state->ghost.addToLevel(state);
}

void goIntoHouse(GameState* state) {
    state->gameMode = INSIDE;
    state->canShoot = false;
    state->insideGhost.x = state->windowWidth/2;
    state->insideGhost.y = state->windowHeight/2;

    clearLevel(state);
    
    state->book.addToLevel(state);
    state->flowerPot.addToLevel(state);
    state->chestEntity.addToLevel(state);
    state->craftingTable.addToLevel(state);
    state->room.lantern.addToLevel(state);
    state->insideGhost.addToLevel(state);
}

void goIntoGreenhouse(GameState* state) {
    state->gameMode = GREENHOUSE;
}   

void goOutside(GameState* state) {
    state->gameMode = OUTSIDE;
    state->canShoot = true;

    genLevel(state->currentLevel, state);
    
    state->ghost.y = state->groundLevel - state->ghost.height;
    
    state->ghost.health = state->ghost.maxHealth;
    state->ghost.dead = false;

    state->ghost.showing = true;

    state->currentMenu = NULL;
}

void goHome(GameState* state) {
    state->currentLevel = 2;
    state->ghost.x = 1200;
    state->ghost.y = 600;
    genLevel(state->currentLevel, state);
}

void myshaClick(GameState* state) {
    state->ghost.x = 1500;
    state->viewX = 1200;
}
