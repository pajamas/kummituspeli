#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

#include "structs.h"

void drawMode(GameState* state, enum modes mode);
void drawOutside(GameState* state);
void drawInside(GameState* state);
void drawGreenhouse(GameState* state);
void drawTetris(GameState* state);
void drawBook(GameState* state);
void drawCave(Cave* cave, GameState* state);
void drawDialogueGamemode(GameState* state);

#endif
