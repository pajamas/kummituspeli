#include "structs.h"
#include "inventory.h"
#include "shopmenu.h"
#include "items.h"

void initShopMenu(GameState* state) {
    ShopMenu* shopMenu = &state->shopMenu;
    shopMenu->bgColor = state->colors.menuBgColorTransparent;
    shopMenu->padding = 15;
    shopMenu->paddingTop = 20;
    shopMenu->buyAmount = 1;
    int padding = shopMenu->padding;

    shopMenu->shopItems = newInventory(9, 1, state);
    shopMenu->buyInventory = newInventory(4, 4, state);
    shopMenu->sellInventory = newInventory(4, 4, state);

    string okText = "ok";
    shopMenu->okButton = (Button) {
        .width = (double) al_get_text_width(state->fonts.font, okText.c_str()) + state->windowHeight / 14,
        .height = (double) state->windowHeight / 14,
        .bgColor = state->colors.buttonBg,
        .activeBgColor = state->colors.buttonActiveBg,
        .disabledBgColor = state->colors.buttonDisabledBg,
        .text = "ok",
    };
    shopMenu->okButton.command = shopMenuOk;
    string cancelText = "cancel";
    shopMenu->cancelButton = (Button) {                
        .width = (double) al_get_text_width(state->fonts.font, cancelText.c_str()) + state->windowHeight / 14,
        .height = (double) state->windowHeight / 14,
        .bgColor = state->colors.buttonBg,
        .activeBgColor = state->colors.buttonActiveBg,
        .disabledBgColor = state->colors.buttonDisabledBg,
        .text = "cancel",
    };
    shopMenu->cancelButton.command = shopMenuCancel;

    shopMenu->trashButton = (Button) {                
        .width = (double) state->windowHeight / 14,
        .height = (double) state->windowHeight / 14,
        .bgColor = state->colors.buttonBg,
        .activeBgColor = state->colors.buttonActiveBg,
        .disabledBgColor = state->colors.buttonDisabledBg,
    };
    shopMenu->trashButton.command = shopMenuTrash;
    shopMenu->trashButton.image = al_load_bitmap("resources/trashicon.png");

    shopMenu->width = shopMenu->shopItems.widthPx + shopMenu->padding*2;
    shopMenu->height = 7*padding + 3*state->fonts.fontSize + shopMenu->shopItems.heightPx
                       + shopMenu->buyInventory.heightPx
                       + shopMenu->okButton.height;
    shopMenu->x = state->windowWidth/2 - shopMenu->width/2;
    shopMenu->y = state->windowHeight/2 - shopMenu->height/2;

    shopMenu->shopItems.x = shopMenu->x + shopMenu->padding;
    shopMenu->shopItems.y = shopMenu->y + state->fonts.fontSize + shopMenu->paddingTop
                            + shopMenu->padding;
    initInventory(&shopMenu->shopItems, false, state);

    shopMenu->buyInventory.x = shopMenu->x + shopMenu->width/2 
                               - shopMenu->buyInventory.widthPx - padding/2;
    shopMenu->buyInventory.y = shopMenu->shopItems.y + shopMenu->shopItems.heightPx
                               + 2*shopMenu->padding + state->fonts.fontSize;
    initInventory(&shopMenu->buyInventory, false, state);

    shopMenu->sellInventory.x = shopMenu->x + shopMenu->width/2 + padding/2;
    shopMenu->sellInventory.y = shopMenu->buyInventory.y;
    initInventory(&shopMenu->sellInventory, false, state);


    shopMenu->trashButton.x = shopMenu->x + padding;
    
    shopMenu->cancelButton.x = shopMenu->x + shopMenu->width - shopMenu->padding
                               - shopMenu->cancelButton.width;
    shopMenu->okButton.x = shopMenu->cancelButton.x - shopMenu->okButton.width
                           - shopMenu->padding;
    shopMenu->okButton.y = shopMenu->sellInventory.y 
                           + shopMenu->sellInventory.heightPx
                           + state->fonts.fontSize
                           + 2*shopMenu->padding;
    shopMenu->trashButton.y = shopMenu->okButton.y;
    shopMenu->cancelButton.y = shopMenu->okButton.y;

    
    addToInventory(&state->shopMenu.shopItems, &state->tiara, 1);
    addToInventory(&state->shopMenu.shopItems, &state->flowerPot2Item, 1);
    addToInventory(&state->shopMenu.shopItems, &state->flowerPot3Item, 1);
    addToInventory(&state->shopMenu.shopItems, &state->strawberryItem, 1);
    FOR(state->shopMenu.shopItems.size) {
        state->shopMenu.shopItems.slots[i].itemsContained.notOwned = true;
    }

    FOR(state->shopMenu.buyInventory.size) {
        state->shopMenu.buyInventory.slots[i].itemsContained.notOwned = true;
    }
}

void drawShopMenu(GameState* state) {
    ShopMenu* menu = &state->shopMenu;
    // BACKGROUND
    al_draw_filled_rectangle(menu->x,menu->y,
                             menu->x+menu->width,menu->y+menu->height, 
                             menu->bgColor);
    // title
    al_draw_text(state->fonts.font, state->colors.textColor, menu->x + menu->padding,
                 menu->y + menu->paddingTop, ALLEGRO_ALIGN_LEFT, "Shop");

    // buy/sell text
    al_draw_text(state->fonts.font, state->colors.textColor, menu->sellInventory.x,
                 menu->shopItems.y + menu->shopItems.heightPx + menu->padding, 
                 ALLEGRO_ALIGN_LEFT, "Sell:");
    al_draw_text(state->fonts.font, state->colors.textColor, menu->buyInventory.x,
                 menu->shopItems.y + menu->shopItems.heightPx + menu->padding, 
                 ALLEGRO_ALIGN_LEFT, "Buy:");

    drawInventory(state, &state->shopMenu.shopItems);
    drawInventory(state, &state->shopMenu.buyInventory);
    drawInventory(state, &state->shopMenu.sellInventory);

    drawButton(state, &state->shopMenu.okButton);
    drawButton(state, &state->shopMenu.cancelButton);
    drawButton(state, &state->shopMenu.trashButton);

    // coin images
    al_draw_bitmap(state->coin.image, menu->sellInventory.x + menu->padding/2,
                   menu->sellInventory.y + menu->buyInventory.heightPx + menu->padding*3/5, 0);
    al_draw_bitmap(state->coin.image, menu->buyInventory.x + menu->padding/2,
                   menu->buyInventory.y + menu->buyInventory.heightPx + menu->padding*3/5, 0);

    // sell text
    al_draw_text(state->fonts.font, state->colors.textColor, 
                 menu->sellInventory.x + menu->padding + al_get_bitmap_width(state->coin.image),
                 menu->sellInventory.y + menu->sellInventory.heightPx + menu->padding, 
                 ALLEGRO_ALIGN_LEFT, to_string(menu->sellPrice).c_str());
    // buy text
    al_draw_text(state->fonts.font, state->colors.textColor, 
                 menu->buyInventory.x + menu->padding + al_get_bitmap_width(state->coin.image),
                 menu->buyInventory.y + menu->buyInventory.heightPx + menu->padding, 
                 ALLEGRO_ALIGN_LEFT, to_string(menu->buyPrice).c_str());
}

bool shopMenuInteract(GameState* state, int button) {
    ALLEGRO_MOUSE_STATE mstate;
    al_get_mouse_state(&mstate);
    if (!(mstate.x > state->shopMenu.x &&
          mstate.x < state->shopMenu.x + state->shopMenu.width &&
          mstate.y > state->shopMenu.y &&
          mstate.y < state->shopMenu.x + state->shopMenu.height)) {
        return false;
    }
    if (inventoryInteract(state, &state->shopMenu.shopItems, button)) {
        return true;
    }
    else if (inventoryInteract(state, &state->shopMenu.buyInventory, button)) {
        int price = 0;
        FOR(state->shopMenu.buyInventory.size) {
            if (state->shopMenu.buyInventory.slots[i].itemsContained.item == NULL) continue;
            price += state->shopMenu.buyInventory.slots[i].itemsContained.amount
                     * state->shopMenu.buyInventory.slots[i].itemsContained.item->buyPrice;
        }
        state->shopMenu.buyPrice = price;
        return true;
    }
    else if (inventoryInteract(state, &state->shopMenu.sellInventory, button)) {
        int price = 0;
        FOR(state->shopMenu.sellInventory.size) {
            if (state->shopMenu.sellInventory.slots[i].itemsContained.item == NULL) continue;
            price += state->shopMenu.sellInventory.slots[i].itemsContained.amount
                     * state->shopMenu.sellInventory.slots[i].itemsContained.item->sellPrice;
        }
        state->shopMenu.sellPrice = price;
        return true;
    }

    else if (buttonInteract(&state->shopMenu.trashButton, state) ||
             buttonInteract(&state->shopMenu.okButton, state) ||
             buttonInteract(&state->shopMenu.cancelButton, state)) return true;
    return true;
}

void nextBuyAmount(GameState* state) {
    state->shopMenu.buyAmount *= 10;
    if (state->shopMenu.buyAmount == 1000) state->shopMenu.buyAmount = 1;
    FOR(state->shopMenu.shopItems.size) {
        if (state->shopMenu.shopItems.slots[i].itemsContained.item != NULL &&
            !state->shopMenu.shopItems.slots[i].itemsContained.item->nonStackable) {
            state->shopMenu.shopItems.slots[i].itemsContained.amount = state->shopMenu.buyAmount;
        }
    }
}

void openShopMenu(GameState* state) {
    state->shopMenu.open = true;
    state->inventory.open = true;
    state->shopMenu.buyAmount = 1;
}

void closeShopMenu(GameState* state) {
    ShopMenu* shopMenu = &state->shopMenu;
    shopMenu->open = false;
    shopMenu->sellPrice = 0;
    shopMenu->buyPrice = 0;
    FOR(shopMenu->sellInventory.size) {
        shopMenu->buyInventory.slots[i].itemsContained.item = NULL;
        shopMenu->buyInventory.slots[i].itemsContained.amount = 0;
        shopMenu->sellInventory.slots[i].itemsContained.item = NULL;
        shopMenu->sellInventory.slots[i].itemsContained.amount = 0;
    }
    if (state->handItem.notOwned) {
        state->handItem.item = NULL;
        state->handItem.amount = 0;
        state->handItem.notOwned = false;
    }
}

void shopMenuOk(GameState* state) {
    int sellPrice = state->shopMenu.sellPrice;
    int buyPrice = state->shopMenu.buyPrice;
    if (sellPrice < buyPrice) {
        int coinsInInventory = 0;
        FOR(state->inventory.size) {
            if (state->inventory.slots[i].itemsContained.item != NULL &&
                state->inventory.slots[i].itemsContained.item == &state->coin) {
                coinsInInventory += state->inventory.slots[i].itemsContained.amount;
            }
        }
        if (coinsInInventory + sellPrice < buyPrice) {
            return;
        } else {
            removeFromInventory(&state->inventory, &state->coin, 
                                buyPrice - sellPrice);
            sellPrice = 0;
        }
    } else {
        sellPrice -= buyPrice;
    }
    FOR(state->shopMenu.buyInventory.size) {
        int added = addToInventory(&state->inventory, state->shopMenu.buyInventory.slots[i].itemsContained.item,
        state->shopMenu.buyInventory.slots[i].itemsContained.amount);
        if (added < state->shopMenu.buyInventory.slots[i].itemsContained.amount) {
            state->shopMenu.buyInventory.slots[i].itemsContained.amount -= added;
            dropItem(state->shopMenu.buyInventory.slots[i].itemsContained, state);
        }
    }
    int added = addToInventory(&state->inventory, &state->coin, sellPrice);
    if (added < sellPrice) {
        StackOfItems drop = (StackOfItems) {
            .item = &state->coin,
            .amount = sellPrice - added
        };
        dropItem(drop, state);
    }

    closeShopMenu(state);
}

void shopMenuCancel(GameState* state) {
    FOR(state->shopMenu.sellInventory.size) {
        addToInventory(&state->inventory, state->shopMenu.sellInventory.slots[i].itemsContained.item,
                       state->shopMenu.sellInventory.slots[i].itemsContained.amount);
    }
    closeShopMenu(state);
}

void shopMenuTrash(GameState* state) {
    if (state->handItem.item == NULL) return;
    if (state->handItem.notOwned) {
        state->handItem.item = NULL;
        state->handItem.amount = 0;
        state->handItem.notOwned = false;
    }
}
