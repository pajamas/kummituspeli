#include "structs.h"
#include <fstream>

void save(GameState* state) {
    {
    std::ofstream os("data.json");
    cereal::JSONOutputArchive archive(os);
    archive( CEREAL_NVP(state->fox), CEREAL_NVP(state->inventory),
             CEREAL_NVP(state->chest));
    }
}
/*
archive( CEREAL_NVP(m1), // Names the output the same as the variable name
             someInt,        // No NVP - cereal will automatically generate an enumerated name
             cereal::make_nvp("this_name_is_way_better", d) ); // specify a name of your choosing
             */
