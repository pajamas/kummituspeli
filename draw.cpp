#include "structs.h"
#include "positions.h"
#include "events.h"
#include "entities.h"
#include "cave.h"
#include "draw.h"
#include "chest.h"
#include "inventory.h"
#include "crafting.h"
#include "items.h"
#include "tetrislogic.h"

void drawMode(GameState* state, enum modes mode) {
    switch (mode) {
        case INSIDE:
            drawInside(state);
            break;
        case GREENHOUSE:
            drawGreenhouse(state);
            break;
        case OUTSIDE:
            drawOutside(state);
            break;
        case CAVE:
            drawCaveGamemode(state);
            break;
        case TETRIS:
            drawTetris(state);
            break;
        default:
            return;
    }
}

int modulo(int x,int N){
    return (x % N + N) %N;
}

// DRAW OUTSIDE STUFF
void drawOutside(GameState* state) {
    // sky
    al_clear_to_color(state->colors.skyColor);

    // stars
    al_draw_bitmap(state->stars1.image, state->stars1.x - (int) state->viewX % 1920,
                   state->stars1.y, 0);
    al_draw_bitmap(state->stars2.image, state->stars2.x - (int) state->viewX % 1920,
                   state->stars2.y, 0);
    al_draw_bitmap(state->stars1.image, state->stars1.x - (int) state->viewX % 1920 + 1920,
                   state->stars1.y, 0);
    al_draw_bitmap(state->stars2.image, state->stars2.x - (int) state->viewX % 1920 + 1920,
                   state->stars2.y, 0);

    float treeHeight = al_get_bitmap_height(state->trees[0].img);
    for (Tree &t: state->trees) {
        al_draw_bitmap(t.img, modulo((int)(t.x - (int) state->viewX), 2100)-100, 
                       state->groundLevel - t.height, 0);
    }
    for (Plant &p: state->plants) {
        al_draw_bitmap(p.img, modulo((int)(p.x - state->viewX), 2100)-40, p.y, 0);
    }

    // ground
    al_draw_filled_rectangle(0,state->groundLevel,state->windowWidth,state->windowHeight, state->colors.groundColor );
    if (state->cave.x < state->viewX + state->windowWidth)
        drawCave(&state->cave, state);

    // entities
    for (Entity* entity: state->toShow) {
        if (entity != NULL)
            entity->draw(state);
    }
    for (Building* building: state->toShowBuildings) {
        building->draw(state);
    }
    for (Animal* animal: state->toShowAnimals) {
        animal->draw(state);
    }
    for (Customer &customer: state->customers) {
        customer.draw(state);
    }
    if (state->ghost.hat != NULL) {
        al_draw_bitmap(state->ghost.hat->image, state->ghost.x + state->ghost.xOffset
                       + state->ghost.width/2 - bitmap_width(state->ghost.hat->image)/2 + 1
                       + (state->ghost.onLevel-1)*state->windowWidth - state->viewX,
                       state->ghost.y + state->ghost.yOffset + 10
                       - bitmap_height(state->ghost.hat->image), 0);
    }
                    
    // items on ground
    FOR(MAX_GROUND_ITEMS) {
        drawItemOnGround(state->itemsOnGround[i], state);
    }

    // piip
    if (state->currentLevel == 1 || state->currentLevel == 2) {
        al_draw_text(state->fonts.font, WHITE, -state->viewX + state->mysha.x + 250, 
                     state->mysha.y + 150, ALLEGRO_ALIGN_CENTRE, "piip :3");
    }

    // level number
    char levelNum[4];
    sprintf(levelNum, "%d", state->currentLevel);
    al_draw_text(state->fonts.font, WHITE, state->windowWidth*9/10, 80, 
                 ALLEGRO_ALIGN_CENTRE, levelNum);
    
    // bubbles
    FOR(MAX_BUBBLES) {
        if (state->bubbles[i].alive)
            drawBubble(&state->bubbles[i]);
    }
}

// DRAW INSIDE STUFF
void drawInside(GameState* state) {
    al_clear_to_color(al_map_rgb(0,0,0)); // black background

    float tileSize = state->room.tileSize;
    float roomX = state->room.x;
    float roomY = state->room.y;
    float floorY = state->room.floorY;

    ALLEGRO_BITMAP *wallTile, *floorTile;

    if (state->room.lightsOn) {
        wallTile = state->room.brightWallTile;
        floorTile = state->room.brightFloorTile;
    } else {
        wallTile = state->room.wallTile;
        floorTile = state->room.floorTile;
    }
    FORI(state->room.wallHeight) {
        FORJ(state->room.width) {
            al_draw_bitmap(wallTile, roomX + j*tileSize, roomY + i*tileSize, 0);
        }
    }
    FORI(state->room.height-state->room.wallHeight) {
        FORJ(state->room.width) {
            al_draw_bitmap(floorTile, roomX + j*tileSize, floorY + i*tileSize, 0);
        }
    }
    float hallwayX = state->room.hallwayX;
    float hallwayY = state->room.hallwayY;
    
    FORI(2) {
        FORJ(2) {
            al_draw_bitmap(floorTile, hallwayX + j*tileSize, hallwayY + i*tileSize, 0);
        }
    }

    // entities
    /*for (Entity* entity: state->toShow) {
	    if (entity != NULL) 
            entity->draw(state);
    }*/
    state->room.lantern.draw(state);

    bool ghost_drawn = false;
    for (int i = 0; i < 6; i++) {
        // DRAW GHOST HERE IF ITS ON NEXT ROW
        if (state->insideGhost.y >= state->room.floorY + (i-1)*state->room.tileSize 
            && state->insideGhost.y < state->room.floorY + i*state->room.tileSize) {
            state->insideGhost.draw(state);
            ghost_drawn = true;
        }
        for (int j = 0; j < 11; j++) {
            if (state->room.furniture[j][i] != NULL) 
                state->room.furniture[j][i]->drawAtCoords(state->room.x+state->room.tileSize*j + 0.5*tileSize - 0.5*state->room.furniture[j][i]->width,
                                            state->room.floorY + state->room.tileSize*i + 0.5*state->room.tileSize-0.5*state->room.furniture[j][i]->height, state);
        }
    }
    if (!ghost_drawn) state->insideGhost.draw(state);
    
    if (state->ghost.hat != NULL) {
        al_draw_scaled_bitmap(state->ghost.hat->image, 0, 0, 
                              bitmap_width(state->ghost.hat->image),
                              bitmap_height(state->ghost.hat->image),
                              state->insideGhost.x + state->insideGhost.width/2
                              - bitmap_width(state->ghost.hat->image) + 1,
                              state->insideGhost.y + 20
                              - bitmap_height(state->ghost.hat->image)*2, 
                              2*bitmap_width(state->ghost.hat->image),
                              2*bitmap_height(state->ghost.hat->image),
                              0);
    }
    
    if (state->chest.open) {
        drawInventory(state, &state->chest);
    } else if (state->craftMenu.open) {
        drawCraftMenu(state);
    }
}

// DRAW INSIDE OF GREENHOUSE
void drawGreenhouse(GameState* state) {
    al_clear_to_color(al_map_rgb(0,0,0)); // black background

    int tileSize = 64;
    int pixelSize = 4;
    int height = (state->windowHeight/tileSize) - 2;
    if (height % 2 == 1) height -= 1;
    int fieldWidth = 15;
    float roomX = 3*tileSize;
    float roomY = state->windowHeight/2 - height*tileSize/2;

    ALLEGRO_BITMAP *plank, *edgePlank, *field;
    plank = state->images.plank;
    edgePlank = state->images.edgePlank;
    field = state->images.field;

    FORI(height) {
        FORJ(3) {
            al_draw_bitmap(plank, roomX + j*tileSize, roomY + i*tileSize, 0);
        }
    }
    FOR(height) {
        al_draw_bitmap(edgePlank, roomX + 3*tileSize, roomY + i*tileSize, 0);
    }
    float fieldX = roomX + 4*tileSize;
    FORI(height) {
        FORJ(fieldWidth) {
            al_draw_bitmap(field, fieldX + j*tileSize, roomY + i*tileSize, 0);
            if (state->fieldTiles[i*fieldWidth + j].plant != NULL) {
                al_draw_bitmap(state->fieldTiles[i*fieldWidth + j].plant->image,
                               fieldX + j*tileSize + pixelSize, 
                               roomY + i*tileSize + 5*pixelSize, 0);
            }
        }
    }
    
    float hallwayX = 0;
    float hallwayY = state->windowHeight/2 - tileSize;
    
    FORI(2) {
        FORJ(3) {
            al_draw_bitmap(plank, hallwayX + j*tileSize, hallwayY + i*tileSize, 0);
        }
    }
}

void drawTetris(GameState* state) {
    float groundLevel = state->windowHeight - state->windowHeight/6;

    al_clear_to_color(state->colors.darkerSkyColor);

    state->stars1.drawImage(state);
    state->stars2.drawImage(state);

    Tree* t = &state->trees[0];
    float treeWidth = al_get_bitmap_width(t->img);
    float treeHeight = al_get_bitmap_height(t->img);
    for (int i=0; i<10;i++) { // draw the trees
        Tree* t = &state->trees[i];
        al_draw_scaled_bitmap(t->img,
        0, 0, treeWidth, treeHeight,
        t->x, groundLevel-treeHeight*3, treeWidth*3, treeHeight*3, 0);
    }
    float plantWidth, plantHeight;
    // draw plants
    FOR(PLANTS_AMOUNT) {
        Plant* p = &state->plants[i];
        plantWidth = al_get_bitmap_width(p->img);
        plantHeight = al_get_bitmap_height(p->img);
        al_draw_scaled_bitmap(p->img,
        0, 0, plantWidth, plantHeight,
        p->x, groundLevel-plantHeight*3, plantWidth*3, plantHeight*3, 0);
    }
    // ground
    al_draw_filled_rectangle(0,groundLevel,1920,1080, state->colors.groundColor );
    
    // ghost
    al_draw_bitmap(state->ghost.image, 400, groundLevel-state->ghost.height-5, 0);
    
    // draw grid
    int blockHeight = state->tetris->blockHeight;
    int gridWidth = state->tetris->gridWidth;
    int gridHeight = state->tetris->gridHeight;
    int gridWidthPx,gridHeightPx;
    gridWidthPx = gridWidth*blockHeight;
    gridHeightPx = gridHeight*blockHeight;

    int gridStartX, gridStartY;
    gridStartX = state->windowWidth/2-gridWidthPx/2;
    gridStartY = groundLevel-gridHeightPx;

    
    al_draw_filled_rectangle(gridStartX-5,gridStartY-5,gridStartX+gridWidthPx+5,
            gridStartY+gridHeightPx,state->colors.skyColor);

    // draw ghostBlock
    float ghostBlockX, ghostBlockY;
    for (int i = 0; i < 4; i++) {
        ghostBlockX = gridStartX+state->tetris->ghostBlock.blocks[i].x*blockHeight;
        ghostBlockY = gridStartY+state->tetris->ghostBlock.blocks[i].y*blockHeight;
        al_draw_rectangle(ghostBlockX, ghostBlockY,
                          ghostBlockX+blockHeight, ghostBlockY+blockHeight,
                          al_map_rgb(245, 212, 237),3);
    }

    // draw tetrominos
    for (int i = 0; i < 200; i++) {
        for (int j = 0; j < 4; j++) {
            if (state->tetris->tetrominos[i].blocks[j].visible) {
                int x = state->tetris->tetrominos[i].blocks[j].x;
                int y = state->tetris->tetrominos[i].blocks[j].y;
                al_draw_rotated_bitmap(state->tetris->tetrominos[i].blocks[j].image,
                    20, 20, gridStartX+x*blockHeight+20, gridStartY+y*blockHeight+20, state->tetris->tetrominos[i].orientation * M_PI/2, 0);
            }
        }
    }

    // score & other text
    char scoreText[100], linesClearedText[100], seedsFoundText[100];
    sprintf(scoreText,"score: %d",state->tetris->score);
    sprintf(linesClearedText,"lines cleared: %d",state->tetris->linesCleared);
    sprintf(seedsFoundText,"seeds found: %d",state->tetris->seedsFound);
    al_draw_text(state->fonts.font, state->colors.textColor, gridStartX+gridWidthPx+50,gridStartY+50,ALLEGRO_ALIGN_LEFT, scoreText);
    al_draw_text(state->fonts.font, state->colors.textColor, gridStartX+gridWidthPx+50,gridStartY+80,ALLEGRO_ALIGN_LEFT, linesClearedText);
    al_draw_text(state->fonts.font, state->colors.textColor, gridStartX+gridWidthPx+50,gridStartY+110,ALLEGRO_ALIGN_LEFT, seedsFoundText);

}

void drawBook(GameState* state) {
    state->openBook.drawImage(state);

    state->bookState.leftPage.drawImage(state);
    state->bookState.rightPage.drawImage(state);

    al_draw_bitmap(state->bookState.xButton.image,
                   state->bookState.xButton.x, state->bookState.xButton.y, 0);
}

void drawCave(Cave* cave, GameState* state) {
    char tile;
    double x = cave->x - state->viewX;
    for (int i = 0; i < cave->height; i++) {
        for (int j = 0; j < cave->width; j++) {
            tile = cave->shape[i*cave->width + j];
            if (tile == 'X' || tile == 'B') {
                al_draw_bitmap(cave->caveWallImg,
                               x + j*cave->tileSize, 
                               cave->y + i*cave->tileSize, 0);
            }
        }
    }
}

void drawDialogueGamemode(GameState* state) {
    drawMode(state, state->previousGameMode);
    Customer* customer = state->customerShowingDialogue;
    // draw background
    al_draw_filled_rectangle(20, state->windowHeight*3/4,
                             state->windowWidth-20, state->windowHeight-20,
                             state->colors.menuBgColorTransparent);
    
    // draw portrait
    al_draw_bitmap(customer->portrait, 
                   40, state->windowHeight*3/4 + 20, 0);
    
    
    // draw text
    al_draw_text(state->fonts.font, state->colors.textColor, 
                 200, state->windowHeight*3/4 + 20,
                 ALLEGRO_ALIGN_LEFT, customer->dialogue.c_str());
}
