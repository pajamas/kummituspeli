#include "structs.h"
#include "inventory.h"
#include "events.h"
#include "chest.h"
#include "other.h"

void openChest(GameState* state) {
    playSound(state->doorSound, 0.7, state);
    state->craftMenu.open = false;
    state->chest.open = true;
    openInventory(state);
    state->chestEntity.onclick = closeChest;
}

void closeChest(GameState* state) {
    playSound(state->doorSound, 0.7, state);
    state->chest.open = false;
    closeInventory(state);
    state->chestEntity.onclick = openChest;
}

