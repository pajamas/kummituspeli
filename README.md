# Kummituspeli

Tässä on kummituspeli tehty c:llä ja allegro-kirjastolla.

![kummitus](resources/ghost.png)

Ubuntu Allegron lataamis ohjeet:

`sudo add-apt-repository ppa:allegro/5.2`

`sudo apt-get install liballegro*5.2 liballegro*5-dev`
