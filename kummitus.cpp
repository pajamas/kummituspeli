//#define must_init(p,s) (!(p) ? puts(s),exit(1) : )

#include "interactions.h"
#include "positions.h"
#include "inventory.h"
#include "levels.h"
#include "draw.h"
#include "cave.h"
#include "events.h"
#include "tetrislogic.h"
#include "gamemodes.h"
#include "chest.h"
#include "entities.h"
#include "command.h"
#include "house.h"
#include "other.h"
#include "shopmenu.h"
#include "customers.h"
#include "crafting.h"
#include <fstream>

#define bitmap(s) al_load_bitmap(s)
#define bitmap_height(s) al_get_bitmap_height(s)
#define bitmap_width(s) al_get_bitmap_width(s)

void must_init(bool p, string s) {
    if (!p) {
        string msg = "could not init" + s;
        puts(msg.c_str());
        exit(1);
    }
}

int main() { 
    srand(time(0));

    must_init(al_init(), "allegro");
    must_init(al_init_primitives_addon(), "primitives");

    // input methods
    must_init(al_install_keyboard(), "keyboard");
    must_init(al_install_mouse(), "mouse");

    // AUDIO
    must_init(al_install_audio(), "audio");
    must_init(al_init_acodec_addon(), "acodec addon");
    al_reserve_samples(2);

    ALLEGRO_SAMPLE* sample = al_load_sample("resources/audio/pop-64k.opus");
    //ALLEGRO_SAMPLE* sample = al_load_sample("resources/audio/pop.wav");
    if (sample == NULL) {
        error("could not load sample");
    }

    // monitor info
    ALLEGRO_MONITOR_INFO aminfo;
    al_get_monitor_info(0,&aminfo);
    int desktop_width = aminfo.x2-aminfo.x1;
    int desktop_height = aminfo.y2-aminfo.y1;

    // display options
    al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
    al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
    al_set_new_bitmap_flags(ALLEGRO_MIN_LINEAR | ALLEGRO_MAG_LINEAR);
    al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);

    // display
    ALLEGRO_DISPLAY* disp = al_create_display(desktop_width, desktop_height);
    must_init(disp, "display");
    
    must_init(al_init_image_addon(), "image addon");

    // cursor
    ALLEGRO_BITMAP* cursorImage = bitmap("resources/cursor.png");
    ALLEGRO_MOUSE_CURSOR* cursor = al_create_mouse_cursor(cursorImage, 0, 0);
    al_set_mouse_cursor(disp, cursor);
    
    // fonts
    must_init(al_init_font_addon(), "font addon"); // initialize the font addon
    al_init_ttf_addon();

    GameState state = (GameState) {
        .timer = al_create_timer(1.0 / 60.0),
        .queue = al_create_event_queue(),
        .gameMode = OUTSIDE,
        .windowHeight = al_get_display_height(disp),
        .windowWidth = al_get_display_width(disp),
	    .done = false,
	    .redraw = true,
        .canShoot = true,
        .soundOn = true,
        .fox = Animal("Fox",1),
        .bee = Animal("Bee",2),
        .bunny = Animal("Bunny",1),
        .cat = Animal("Cat",2),
        .newestItemID = -1,
	    .currentLevel = 1,
        .groundLevel = (double) al_get_display_height(disp)*604/1080,
	    .plantspng = {
        	al_load_bitmap("resources/plant0.png"),
        	al_load_bitmap("resources/plant1.png"),
        	al_load_bitmap("resources/plant2.png"),
        	al_load_bitmap("resources/plant3.png"),
        	al_load_bitmap("resources/plant4.png"),
        	al_load_bitmap("resources/plant5.png"),
        	al_load_bitmap("resources/plant6.png"),
        	al_load_bitmap("resources/plant7.png"),
	    },
	    .flowerPotStages = {	
            {al_load_bitmap("resources/flowerpot1.png"),
        	al_load_bitmap("resources/flowerpot2.png"),
   	    	al_load_bitmap("resources/flowerpot3.png"),
        	al_load_bitmap("resources/flowerpot4.png"),
        	al_load_bitmap("resources/flowerpot5.png")
            },{
            al_load_bitmap("resources/flowerpot2_1.png"),
            al_load_bitmap("resources/flowerpot2_2.png"),
            al_load_bitmap("resources/flowerpot2_3.png"),
            al_load_bitmap("resources/flowerpot2_4.png"),
            al_load_bitmap("resources/flowerpot2_5.png"),
            },{
            al_load_bitmap("resources/flowerpot3_1.png"),
            al_load_bitmap("resources/flowerpot3_2.png"),
            al_load_bitmap("resources/flowerpot3_3.png"),
            al_load_bitmap("resources/flowerpot3_4.png"),
            al_load_bitmap("resources/flowerpot3_5.png"),
            }
	    },
        .flowerItemImages = {
            al_load_bitmap("resources/floweritem.png"),
            al_load_bitmap("resources/floweritem2.png"),
            al_load_bitmap("resources/floweritem3.png"),
        },
        .bookPages = {
            al_load_bitmap("resources/bookpage1.png"),
            al_load_bitmap("resources/bookpage2.png"),
            al_load_bitmap("resources/bookpage3.png"),
        },
        .growTimer = al_create_timer(1.0/1.1),
        //.growTimer = al_create_timer(0.05),
        .bubbleTimer = al_create_timer(0.15),
        .bubbleImg = al_load_bitmap("resources/colorfulbubble.png"),
        .bubbleSpeed = 5,
        .tetrisBlock = al_load_bitmap("resources/tetrisblock.png"),
        .tetrisBlocks = al_load_bitmap("resources/tetrisblocks.png"),
        .commandText = "",

        .entities = {{&state.mysha, &state.seed, &state.cave.bats[0]},
                    {&state.house, &state.house2, &state.greenhouse, &state.door,
                        &state.fairy}},
                      
        .animals = {{&state.fox, &state.bunny},
                    {&state.bee, &state.cat}},
        .bubblePop = sample,
        .doorSound = al_load_sample("resources/audio/door.wav"),
    };

    state.fonts = (Fonts) {
        .font = al_load_ttf_font("resources/font/dogicapixel.ttf", 20,0),
        .fontMedium = al_load_ttf_font("resources/font/dogicapixel.ttf", 17.5,0),
        .fontSmall = al_load_ttf_font("resources/font/dogicapixel.ttf", 15,0),
        .fontSize = 20,
        .fontSizeMedium = 17.5, 
        .fontSizeSmall = 15,
    };

    state.images = (Images) {
        .bat = bitmap("resources/bat.png"),
        .spider = bitmap("resources/spider.png"),
        .appleTree = bitmap("resources/appletree.png"),
        .strawberryPlant = bitmap("resources/strawberryplant.png"),
        .damagedSpider = bitmap("resources/damagedspider.png"),
        .deadSpider = bitmap("resources/deadspider.png"),
        .emptyFlowerPot = al_load_bitmap("resources/flowerpot0.png"),
        .lanternOn = bitmap("resources/lantern_on.png"),
        .lanternOff = bitmap("resources/lantern.png"),
        .treeImg = al_load_bitmap("resources/tree.png"),
        .gemItems = {
            bitmap("resources/gemitem1.png"),
            bitmap("resources/gemitem2.png"),
            bitmap("resources/gemitem3.png"),
            bitmap("resources/gemitem4.png"),
        },
        .gems = {
            bitmap("resources/gems1.png"),
            bitmap("resources/gems2.png"),
            bitmap("resources/gems3.png"),
            bitmap("resources/gems4.png"),
        },
	    .inventoryHover = bitmap("resources/invhover.png"),
	    .heart = bitmap("resources/heart.png"),
	    .cross = bitmap("resources/cross.png"),
        .ghostNormal = bitmap("resources/ghost.png"),
        .ghostLeft = bitmap("resources/ghostlookingleft.png"),
        .ghostRight = bitmap("resources/ghostlookingright.png"),
        .plank = bitmap("resources/normalplank.png"),
        .edgePlank = bitmap("resources/edgeplank.png"),
        .field = bitmap("resources/field.png"),
        .customer1 = bitmap("resources/woman1.png"),
        .customer1portrait = bitmap("resources/woman1portrait.png"),
    };

    state.mineralItems[0] = Item("Silver", "resources/silveritem.png", &state);
    state.mineralItems[0].isMineral = true;
    state.mineralItems[0].setPrices(10, 8);
    state.allItems.push_back(&state.mineralItems[0]);

    state.mineralItems[1] = Item("Gold", "resources/golditem.png", &state);
    state.mineralItems[1].isMineral = true;
    state.mineralItems[1].setPrices(15, 12);
    state.allItems.push_back(&state.mineralItems[1]);

    state.minerals[0] = (Mineral) {
        .name = "Silver",
        .symbol = 's',
        .image = al_load_bitmap("resources/cavewallsilver.png"),
        .item = &state.mineralItems[0],
    };

    state.minerals[1] = (Mineral) {
        .name = "Gold",
        .symbol = 'g',
        .image = al_load_bitmap("resources/cavewallgold.png"),
        .item = &state.mineralItems[1],
    };

    state.cave = (Cave) {
        .shape = "_____XXX_____"
                 "___XXXXXBXX__"
                 "_XXXXXXXXXX__"
                 "XXXXXXXXXXXX_"
                 "_XXXXXXXXXXX_"
                 "___XXXXXXXX__",
        .width = 13,
        .height = 6,
        .x = (float) state.windowWidth * 3/10,
        .y = state.groundLevel + 100,
        .caveWallImg = al_load_bitmap("resources/cavewall.png"),
    };
    state.cave.tileSize = al_get_bitmap_width(state.cave.caveWallImg);

    state.bigCave = (Cave) {
        .width = (int) state.windowWidth*27/1920,
        .height = (int) state.windowHeight*27/1080,
        .caveWallImg = al_load_bitmap("resources/cavewall.png"),
        .backButton = (Button) {
            .x = (float) state.windowWidth / 2 - 10,
            .y = 30,
            .width = 20,
            .height = 17,
            .command = leaveCave,
        },
    };

    state.bigCave.shape.resize(state.bigCave.width * state.bigCave.height);
    state.bigCave.tileSize = al_get_bitmap_width(state.bigCave.caveWallImg);
    state.bigCave.x = (float) state.windowWidth / 2 - (state.bigCave.width * state.bigCave.tileSize) / 2;
    state.bigCave.y = (float) state.windowHeight / 2 - (state.bigCave.height * state.bigCave.tileSize) / 2;

    state.bigCave.ghost.width = state.bigCave.tileSize - 4;
    state.bigCave.ghost.height = state.bigCave.tileSize - 4;

    state.bigCave.gem = (Entity) {
        .clickable = true,
        .pickupable = true,
    };

    FOR(MAX_BATS) {
        state.bigCave.bats[i] = newBat(&state);
    }

    state.cave.bats[0] = newBat(&state);
    state.cave.bats[0].onLevel = 1;
    state.cave.bats[0].onclick = goIntoCave;
    addEntityToCave(&state.cave, &state.cave.bats[0]);

    state.bookState = (BookState) {
        .containedInBook = {0},
        .currentPage = 1,
    };

    state.openBook = (Entity) {
        .type = "Open book",
        .image = al_load_bitmap("resources/book2.png"),
    };
    state.openBook.makeSize();
    state.openBook.x = (float) state.windowWidth/6;
    state.openBook.y = state.windowHeight/2 - state.openBook.height/2;

    state.bookState.xButton.x = state.openBook.x + 440;
    state.bookState.xButton.y = state.openBook.y + 20;
    state.bookState.xButton.image = al_load_bitmap("resources/x.png");
    state.bookState.xButton.width = bitmap_width(state.bookState.xButton.image);
    state.bookState.xButton.height = bitmap_height(state.bookState.xButton.image);
    state.bookState.leftButton.image = al_load_bitmap("resources/bookleftarrow.png");
    state.bookState.rightButton.image = al_load_bitmap("resources/bookrightarrow.png");

    state.bookState.leftPage.xOffset = 45;
    state.bookState.leftPage.yOffset = 32;
    state.bookState.rightPage.xOffset = 15;
    state.bookState.rightPage.yOffset = 32;
    state.bookState.leftPage.y = state.openBook.y;
    state.bookState.leftPage.x = state.openBook.x;
    state.bookState.rightPage.y = state.openBook.y;
    state.bookState.rightPage.x = state.openBook.x
                                  + al_get_bitmap_width(state.openBook.image) / 2;
    
    state.bookState.leftPage.width = al_get_bitmap_width(state.openBook.image)/2-50;
    state.bookState.rightPage.width = al_get_bitmap_width(state.openBook.image)/2-20;
    state.bookState.leftPage.height = al_get_bitmap_height(state.openBook.image)-40;
    state.bookState.rightPage.height = al_get_bitmap_height(state.openBook.image)-40;

    state.room = (Room) {
        .width = (int) round(state.windowWidth*11/1920),
        .height = (int) round(state.windowHeight*8/1080),
        .wallHeight = 2,
        .hallwayWidth = 2,
        .wallTile = bitmap("resources/walltile.png"),
        .floorTile = bitmap("resources/floortile.png"),
        .brightWallTile = bitmap("resources/brightwalltile.png"),
        .brightFloorTile = bitmap("resources/brightfloortile.png"),
    };
    state.room.tileSize = al_get_bitmap_width(state.room.wallTile);
    state.room.x = (float) (state.windowWidth / 2 - (state.room.width*state.room.tileSize)/2);
    state.room.y = (float) state.windowHeight / 2 - (state.room.height*state.room.tileSize)/2;
    state.room.floorY = state.room.y + state.room.wallHeight * state.room.tileSize;
    state.room.hallwayHeight = 3;
    state.room.hallwayX = (float) state.windowWidth/2 - (state.room.hallwayWidth*state.room.tileSize)/2;
    state.room.hallwayY = state.room.y + state.room.tileSize*state.room.height;
    state.room.lantern = (Entity) {
        .type = "Lantern",
        .x = (float) state.room.x + state.room.tileSize*(state.windowWidth*2.4/1920),
        .y = (float) (state.room.y + state.room.tileSize*0.3),
        .image = state.images.lanternOff,
        .clickable = true,
        .onclick = toggleLights,
    };
    state.room.lantern.makeSize();

    state.book = (Entity) {
    	.type = "Book",
    	.x = state.room.x + state.room.tileSize*state.room.width*3/8,
    	.y = state.room.y + state.room.tileSize*state.room.height*5/8,
    	.image = al_load_bitmap("resources/book.png"),
        .pickupable = true,
        .item = &state.bookItem,
    };
    state.book.makeDialogue("it's a book!", &state);
    state.book.makeSize();

    state.colors = (Colors) {
        // orangey
        //
        //.skyColor = al_map_rgb(229,195,187),
        //.groundColor = al_map_rgb(60,50,75),
        
        // more desaturated
        //
        //.skyColor = al_map_rgb(210,165,220),
        //
        //.skyColor = al_map_rgb(220,165,230),
        .skyColor = al_map_rgb(235,171,235),
        //.groundColor = al_map_rgb(65,60,90),
        //.menuBgColorTransparent = al_map_rgba(131,90,140,167),

        //.skyColor = al_map_rgb(215,160,225),
        .darkerSkyColor = al_map_rgb(215, 150, 220),
        .groundColor = al_map_rgb(60,55,85),
        .textColor = al_map_rgb(255,255,230),
        .menuBgColor = al_map_rgb(186, 134, 202),
        .menuBgColorTransparent = al_map_rgba(133,88,142,167),
        //.buttonBg = al_map_rgb(255, 187, 237),
        .buttonBg = al_map_rgb(255, 179, 233),
        //.buttonActiveBg = al_map_rgb(255, 199, 231),
        .buttonActiveBg = al_map_rgb(252, 190, 225),
        .buttonDisabledBg = al_map_rgb(222, 196, 218),
        .inventoryBg = al_map_rgba(112,84,126,0.5),
        .inventorySlotBg = al_map_rgb(140,106,157),
    };
    

    state.escMenu = newMenu(&state);
    Button escMenuButton1 = newButton(&state, "resume");
    Button escMenuButton2 = newButton(&state, "tetris");
    Button escMenuButton3 = newButton(&state, "settings");
    Button escMenuButton4 = newButton(&state, "quit game");
    escMenuButton1.command = resumeGame;
    escMenuButton2.command = startTetris;
    escMenuButton3.command = goToSettings;
    escMenuButton4.command = quitGame;
    state.escMenu.buttons[0] = &escMenuButton1;
    state.escMenu.buttons[1] = &escMenuButton2;
    state.escMenu.buttons[2] = &escMenuButton3;
    state.escMenu.buttons[3] = &escMenuButton4;

    //state.escMenu.text = "menu";
    addTextToMenu(&state.escMenu, "menu");
    autoSizeMenu(&state, &state.escMenu);

    state.settingsMenu = newMenu(&state);
    addTextToMenu(&state.settingsMenu, "settings");
    //state.settingsMenu.text = "settings";

    Button settings1 = newButton(&state, "sound off");
    settings1.command = soundToggle;
    Button settings2 = newButton(&state, "video settings");
    settings2.disabled = true;
    Button settings3 = newButton(&state, "controls");
    settings3.command = goToControlsMenu;
    Button settings4 = newButton(&state, "back");
    settings4.command = goToPauseMenu;
    state.settingsMenu.buttons[0] = &settings1;
    state.settingsMenu.buttons[1] = &settings2;
    state.settingsMenu.buttons[2] = &settings3;
    state.settingsMenu.buttons[3] = &settings4;
    autoSizeMenu(&state, &state.settingsMenu);

    state.controlsMenu = newMenu(&state);
    Button controls1 = newButton(&state, "back");
    controls1.command = goToSettings;
    addTextToMenu(&state.controlsMenu,"move: WASD or arrow keys\n"
                                      "shoot: space\n"
                                      "interact: mouse click\n"
                                      "open inventory: E\n"
                                      "drop item from hand: Q");
    state.controlsMenu.buttons[0] = &controls1;
    autoSizeMenu(&state, &state.controlsMenu);

    tetrisState tetris = (struct tetrisState) {
        .done = true,
        .gridHeight = 20,
        .gridWidth = 10,
        .blockHeight = 40,
        .score = 0,
        .seedsFound = 0,
        .tetrisTimer = al_create_timer(1.0 / 1.5),
    };

    state.tetris = &tetris;

    state.tetris->endMenu = newMenu(&state);
    addTextToMenu(&state.tetris->endMenu, "game over");

    state.tetris->escMenu = newMenu(&state);
    addTextToMenu(&state.tetris->escMenu, "paused");

    Button tetrisEscMenuButton = newButton(&state, "resume");
    state.tetris->escMenu.buttons[0] = &tetrisEscMenuButton;
    state.tetris->escMenu.buttons[0]->command = resumeGame;
    Button tetrisEscMenuButton2 = newButton(&state, "restart game");
    state.tetris->escMenu.buttons[1] = &tetrisEscMenuButton2;
    state.tetris->escMenu.buttons[1]->command = restartTetris;
    Button tetrisEscMenuButton3 = newButton(&state, "stop playing");
    state.tetris->escMenu.buttons[2] = &tetrisEscMenuButton3;
    state.tetris->escMenu.buttons[2]->command = goOutside;

    autoSizeMenu(&state, &state.tetris->escMenu);


    state.tetris->endOkButton = newButton(&state, "ok");
    state.tetris->endOkButton.y = state.tetris->endMenu.y + 150;
    state.tetris->endOkButton.command = goOutside;
    state.tetris->endMenu.buttons[0] = &state.tetris->endOkButton;

    autoSizeMenu(&state, &state.tetris->endMenu);

    FOR(PLANT_SPECIES_NUM) {
	    char s[100];
	    sprintf(s,"plant%d.png",i);
	    must_init(state.plantspng[i],s);
    }

    FOR(FLOWER_POT_STAGES_NUM) {
	    must_init(state.flowerPotStages[i],"flower pot stages");
    }

    calculatePlantPositions(&state);

    al_register_event_source(state.queue, al_get_keyboard_event_source());
    al_register_event_source(state.queue, al_get_display_event_source(disp));
    al_register_event_source(state.queue, al_get_timer_event_source(state.timer));
    al_register_event_source(state.queue, al_get_timer_event_source(state.bubbleTimer));
    al_register_event_source(state.queue, al_get_timer_event_source(state.growTimer));
    al_register_event_source(state.queue, al_get_timer_event_source(state.tetris->tetrisTimer));
    al_register_event_source(state.queue, al_get_mouse_event_source());

    state.ghost = Ghost();
    state.ghost.x = 300;
    state.ghost.y = (double) state.windowHeight / 2;
    state.ghost.speed = 4.1;
    state.ghost.jumpHeight = 6.5;
    state.ghost.health = 100;
    state.ghost.maxHealth = 100;
    state.ghost.damageAmount = 5;
    state.ghost.image = state.images.ghostNormal;
    state.ghost.showing = true;
    state.ghost.onLevel = 1;
    state.ghost.makeSize();
    state.ghost.yOffset = -5;
    
    state.insideGhost = Ghost();
    state.insideGhost.image = bitmap("resources/insideghost.png");
    state.insideGhost.makeSize();
    
    state.stars1 = (Entity) {
        .y = 0,
        .image = al_load_bitmap("resources/stars.png"),
        .showing = true,
    };

    state.stars2 = (Entity) {
        .y = -600,
        .image = al_load_bitmap("resources/stars.png"),
        .showing = true,
    };
   
    state.seed = (Entity) {
        .type = "Seed",
	    //.x = (float) state.windowWidth * 2 / 3,
        .y = (float) state.groundLevel-604+575,
        .image = al_load_bitmap("resources/seed2.png"),
        .showing = true,
        .pickupable = true,
        .item = &state.seedItem,
        .onLevel = 1,
    };
    state.seed.makeSize();
    state.seed.makeDialogue("it's a seed!", &state);
    state.seed.makeX(1920 * 2 / 3);

    state.bee = Animal("Bee", 2);
    state.bee.y = (float) state.windowHeight / 4;
    state.bee.loadImage("resources/bee.png");
    state.bee.makeSize();
    state.bee.makeDialogue("bzzzzzzzzz", &state);
    state.bee.makeX(1920 / 2 - 15);

    state.house = (Entity) {
	    .type = "House",
	    .y = (float) state.groundLevel+302-604,
	    .image = al_load_bitmap("resources/house.png"),
        .onLevel = 2,
    };
    state.house.makeSize();
    state.house.makeX(1920 * 1 / 8);

    state.door = (Entity) {
        .type = "Door",
        .x = state.house.x + 81,
        .y = state.house.y + 237,
        .width = 36,
        .height = 66,
        .clickable = true,
        .onclick = goIntoHouse,
        .sound = state.doorSound,
        .onLevel = 2,
    };

    state.house2 = (Entity) {
        .type = "House",
        .image = bitmap("resources/house2.png"),
        .onLevel = 2
    };
    state.house2.makeSize();
    state.house2.y = state.groundLevel - state.house2.height;
    state.house2.makeX(1920 * 1/2);

    state.greenhouse = (Entity) {
	    .type = "Greenhouse",
	    .image = al_load_bitmap("resources/greenhouse.png"),
        .clickable = true,
        //.onclick = goIntoGreenhouse,
        .onLevel = 2,
    };
	state.greenhouse.y = (float) state.groundLevel-bitmap_height(state.greenhouse.image);
    state.greenhouse.makeSize();
    state.greenhouse.makeX(1920 * 1.5 / 4);

    state.fox = Animal("Fox", 1);
    state.fox.speed = 2.5;
    state.fox.jumpHeight = 6.5;
    state.fox.loadImage("resources/fox2.png");
    state.fox.normalImage = state.fox.image;
    state.fox.asleepImage = bitmap("resources/sleepingfox.png");
    state.fox.setOnClick(foxInteract);
    state.fox.feedable = true;
    state.fox.likesAllFood = true;
    state.fox.y = state.groundLevel - al_get_bitmap_height(state.fox.image);
    state.fox.makeDialogue("heheheheh", &state);
    state.fox.makeSize();
    state.fox.makeX(1920 * 4 / 5);
    initTimer(&state.fox.emoteTimer, 1.2, &state);

    state.bunny = Animal("Bunny", 1);
    state.bunny.makeX(1920 * 2 / 5);
    state.bunny.speed = 2.5;
    state.bunny.jumpHeight = 3.5;
    state.bunny.loadImage("resources/bunny.png");
    state.bunny.feedable = true;
    state.bunny.tameable = true;
    state.bunny.favoriteFood = &state.strawberryItem;
    state.bunny.y = state.groundLevel - state.bunny.height;
    state.bunny.makeDialogue("bunny", &state);
    initTimer(&state.bunny.emoteTimer, 1.5, &state);

    state.cat = Animal("Cat", 2);
    state.cat.loadImage("resources/cat.png");
    state.cat.y = state.groundLevel - al_get_bitmap_height(state.cat.image);
    state.cat.makeDialogue("meow", &state);
    state.cat.makeX(1920 * 2 / 5);
    
    state.fairy = (Entity) {
        .x = state.house.x + 100,
        .y = state.house.y + 145,
        .image = al_load_bitmap("resources/fairyinshadow.png"),
        .clickable = true,
        .onclick = openShopMenu,
        .onLevel = 2,
    };

    state.flowerPot = (Entity) {
   	    .type = "Flower Pot",
	    .x = (float) state.room.x + state.room.tileSize*state.room.width*6/8,
    	.y = (float) state.room.y + state.room.tileSize*state.room.height*7/9,
        .yOffset = (float) -al_get_bitmap_height(state.images.emptyFlowerPot),
	    .image = state.images.emptyFlowerPot,
	    .harvestedImage = state.images.emptyFlowerPot,
    	.dialogueStageNum = 0,
        .onclick = flowerPotInteract,
    };
    state.flowerPot.makeDialogue("flower pot", &state);
    state.flowerPot.makeSize();
    state.room.furniture[7][4] = &state.flowerPot;

    state.flowerPot2 = (Entity) {
   	    .type = "Flower Pot",
	    .image = bitmap("resources/flowerpot_2.png"),
    	.dialogueStageNum = 0,
    };
    state.flowerPot2.harvestedImage = state.flowerPot2.image,
	state.flowerPot2.normalImage = state.flowerPot2.image;
    state.flowerPot2.yOffset = (float) -al_get_bitmap_height(state.flowerPot2.image),
    state.flowerPot2.makeSize();

    state.flowerPot2Item = Item("Flower Pot", "resources/flowerpot_2_small.png", &state);
    state.flowerPot2Item.setPrices(60, 40);
    state.allItems.push_back(&state.flowerPot2Item);

    state.flowerPot3 = (Entity) {
        .type = "Flower Pot",
        .image = bitmap("resources/flowerpot_3.png"),
        .dialogueStageNum = 0,
    };
    state.flowerPot3.harvestedImage = state.flowerPot3.image,
    state.flowerPot3.normalImage = state.flowerPot3.image;
    state.flowerPot3.yOffset = (float) -al_get_bitmap_height(state.flowerPot3.image),
    state.flowerPot3.makeSize();

    state.flowerPot3Item = Item("Flower Pot", "resources/flowerpot_3_small.png", &state);
    state.flowerPot3Item.setPrices(40, 30);
    state.allItems.push_back(&state.flowerPot3Item);
            
    state.craftingTable = (Entity) {
        .type = "Crafting Table",
        .x = (float) state.room.x + state.windowWidth*250/1920,
        .y = (float) state.room.floorY + state.windowHeight*50/1080,
        .clickable = true,
        .onclick = openCraftingTable,
    };
    state.craftingTable.loadImage("resources/craftingtable.png");
    initCraftMenu(&state);
    state.room.furniture[2][1] = &state.craftingTable;

    state.chestEntity = (Entity) {
        .type = "Chest",
        .x = (float) state.room.x + state.windowWidth*95/1920,
        .y = (float) state.room.floorY + state.windowHeight*50/1080,
        .image = bitmap("resources/chest.png"),
        .clickable = true,
        .onclick = openChest,
    };
    state.chestEntity.makeSize();
    state.room.furniture[1][1] = &state.chestEntity;

    /*
    state.backArrow = (Entity) {
        .type = "Arrow Button",
	    .x = 50, .y = 50,
	    .image = bitmap("resources/nuoli.png"),
	    .clickable = true,
    };
    state.backArrow.makeSize();
    */

    state.seedItem = Item("Seed", "resources/seed2.png", &state);
    state.seedItem.isFood = true;
    state.seedItem.setPrices(2,1);
    state.allItems.push_back(&state.seedItem);

    state.coin = Item("Coins", "resources/coin.png", &state);
    state.coin.setPrices(1,1);
    state.allItems.push_back(&state.coin);

    state.strawberryItem = Item("Strawberry", "resources/strawberry.png", &state);
    state.strawberryItem.isFood = true;
    state.strawberryItem.setPrices(8,5);
    state.allItems.push_back(&state.strawberryItem);

    state.apple = Item("Apple", "resources/apple.png", &state);
    state.apple.isFood = true;
    state.apple.setPrices(10,7);
    state.allItems.push_back(&state.apple);

    FOR(PLANTABLE_SPECIES) {
        state.flowerItems[i] = Item("Flower", state.flowerItemImages[i], &state);
        state.flowerItems[i].flowerID = i;
        state.flowerItems[i].isFlower = true;
        state.flowerItems[i].bookPageImage = state.bookPages[i];
        state.flowerItems[i].setPrices(-1,1);
        state.allItems.push_back(&state.flowerItems[i]);
    }

    FOR(GEMS) {
        state.gemItems[i] = Item("Gem", state.images.gemItems[i], &state);
        state.gemItems[i].isGem = true;
        state.gemItems[i].setPrices(15,10);
        state.allItems.push_back(&state.gemItems[i]);
    }

    state.bookItem = Item("Book", "resources/bookitem.png", &state);
    state.bookItem.nonStackable = true;
    state.bookItem.onclick = toggleBookOpen;
    state.allItems.push_back(&state.bookItem);

    state.tiara = Item("Tiara", "resources/tiara.png", &state);
    state.tiara.nonStackable = true;
    state.tiara.setPrices(50, 30);
    state.allItems.push_back(&state.tiara);

    state.mysha = (Entity) {
	    .type = "Mysha",
	    .x = (float) state.windowWidth*300/1920,
	    .y = (float) state.windowHeight*50/1080,
	    .image = al_load_bitmap("resources/mysha.png"),
	    .clickable = true,
        .onclick = myshaClick,
        .onLevel = 1,
    };
    state.mysha.makeSize();

    /*
    state.pumpkinItem = (Item) {
        .name = "Pumpkin",
        .ID = newItemID(&state.newestItemID),
        .image = al_load_bitmap("resources/pumpkin.png"),
        .buyPrice = 30,
        .sellPrice = 20,
    };

    state.potatoItem = (Item) {
        .name = "Potato",
        .ID = newItemID(&state.newestItemID),
        .image = al_load_bitmap("resources/pumpkin.png"),
        .buyPrice = 15,
        .sellPrice = 10,
    };

    state.farmablePlants[0] = (FarmablePlant) {
        .name = "Pumpkin",
        .image = bitmap("resources/pumpkin.png"),
        .item = &state.pumpkinItem,
    };
    state.farmablePlants[1] = (FarmablePlant) {
        .name = "Potato",
        .image = bitmap("resources/potatoplant.png"),
        .item = &state.potatoItem,
    };

    state.fieldTiles[17].plant = &state.farmablePlants[0];
    state.fieldTiles[55].plant = &state.farmablePlants[1];
*/
    state.inventory = newInventory(5, 3, &state);
    state.inventory.x = (float) state.windowWidth 
                        - (state.inventory.width+1)*state.inventory.padding 
                        - state.inventory.width*state.inventory.slotWidth;
    state.inventory.y = (float) state.windowHeight
                        - (state.inventory.height+1)*state.inventory.padding
                        - state.inventory.height*state.inventory.slotWidth;
    initInventory(&state.inventory, false, &state);

    state.chest = newInventory(9, 3, &state);
    initInventory(&state.chest, true, &state);


    initShopMenu(&state);


    genLevel(1, &state);

    spawnCustomer(&state);

    //addToInventory(&state.inventory, &state.strawberryItem, 10);
    //addToInventory(&state.inventory, &state.coin, 10000);
   
    memset(state.key, 0, sizeof(state.key));
    
    /*{
        std::ifstream is("data.json");
        // check if file is empty or not open
        if (is.peek() != std::ifstream::traits_type::eof()) {
            cereal::JSONInputArchive archive(is);

            archive( state.fox, state.inventory, state.chest );
            state.inventory.loadItems(&state);
            state.chest.loadItems(&state);
        }
    }*/

    al_start_timer(state.timer);
    while (1)
    {
        al_wait_for_event(state.queue, &state.event);
	    handleEvents(&state);
	
	    if (state.done)
            break;

        if (state.redraw && al_is_event_queue_empty(state.queue))
        {
            switch (state.gameMode) {
                case OUTSIDE:
                    drawOutside(&state);
                    break;
                case INSIDE:
                    drawInside(&state);
                    break;
                case DIALOGUE:
                    drawDialogueGamemode(&state);
                    break;
                case GREENHOUSE:
                    drawGreenhouse(&state);
                    break;
                case CAVE:
                    drawCaveGamemode(&state);
                    break;
                case PAUSED:
                    drawMode(&state, state.previousGameMode);
                    if (state.openBook.showing) {
                        drawBook(&state);
                    }
                    drawMenu(&state, state.currentMenu);
                    break;
                case TETRIS:
                    drawTetris(&state);
                    break;
                case COMMAND:
                    drawCommandLine(&state);
                    break;
            }
            if (state.shopMenu.open) {
                drawShopMenu(&state);
            }
	        if (state.inventory.open) {
		        drawInventory(&state, &state.inventory);
	        }
            if (state.gameMode != PAUSED) {
	            if (state.openBook.showing)
                    drawBook(&state);

                // draw item in hand
	            if (state.handItem.item != NULL) {
		            ALLEGRO_MOUSE_STATE mstate; 
   	 	            al_get_mouse_state(&mstate);
                    int xOffset = bitmap_width(state.handItem.item->image) + 2;
                    int yOffset = bitmap_height(state.handItem.item->image) + 2;
		            al_draw_bitmap(state.handItem.item->image,mstate.x-xOffset,mstate.y-yOffset,0);
                    if (state.handItem.amount > 1) {
                        char amount[11];
                        sprintf(amount, "%d", state.handItem.amount);
                        al_draw_text(state.fonts.fontMedium, state.colors.textColor,
                                     mstate.x + 3, mstate.y - 20,
                                     ALLEGRO_ALIGN_LEFT, amount);
	                }
                }
            }
	        al_flip_display();
            state.redraw = false;
        }
    }

    al_destroy_font(state.fonts.font);
    al_destroy_font(state.fonts.fontSmall);
    al_destroy_font(state.fonts.fontMedium);
    al_destroy_timer(state.timer);
    al_destroy_timer(state.bubbleTimer);
    al_destroy_event_queue(state.queue);
    al_destroy_bitmap(state.bubbleImg);
    al_destroy_bitmap(state.insideGhost.image);
    al_destroy_bitmap(state.ghost.image);
    al_destroy_bitmap(state.stars1.image);
    al_destroy_bitmap(state.seed.image);
    al_destroy_bitmap(state.bee.image);
    al_destroy_bitmap(state.house.image);
    al_destroy_bitmap(state.fox.image);
    al_destroy_bitmap(state.book.image);
    al_destroy_bitmap(state.bookState.xButton.image);
    
    FOR(PLANT_SPECIES_NUM) {
    	al_destroy_bitmap(state.plantspng[i]);
    }
    for (int i = 0; i < PLANTABLE_SPECIES; i++) {
        for (int j = 0; j < FLOWER_POT_STAGES_NUM; j++) {
    	    al_destroy_bitmap(state.flowerPotStages[i][j]);
        }
    }
    return 0;
}
