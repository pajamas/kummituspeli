#ifndef INTERACTIONS_H_INCLUDED
#define INTERACTIONS_H_INCLUDED

#include "structs.h"

void removeFromHand(StackOfItems* handItem, int amount);
void flowerPotInteract(GameState* state);
void grow(GameState* state);
void toggleBookOpen(GameState* state);
void bookInteract(GameState* state, Entity* page);
void foxInteract(GameState* state);

#endif
