#include "house.h"
#include "levels.h"
#include "other.h"

void moveInRoom(GameState* state) {
    float hSpeed = 5.5;
    float vSpeed = 4.9;
    // directions
    if ((state->key[ALLEGRO_KEY_LEFT] | state->key[ALLEGRO_KEY_A]) &&
            !roomCollisions(state, LEFT, -hSpeed))
        state->insideGhost.x -= hSpeed;

    if ((state->key[ALLEGRO_KEY_RIGHT] | state->key[ALLEGRO_KEY_D]) &&
            !roomCollisions(state, RIGHT, hSpeed))
        state->insideGhost.x += hSpeed;
    
    if ((state->key[ALLEGRO_KEY_DOWN] | state->key[ALLEGRO_KEY_S]) &&
            !roomCollisions(state, DOWN, vSpeed))
        state->insideGhost.y += vSpeed;
    
    if ((state->key[ALLEGRO_KEY_UP] | state->key[ALLEGRO_KEY_W]) &&
            !roomCollisions(state, UP, -vSpeed))
        state->insideGhost.y -= vSpeed;
    
    // out of house
    if (state->insideGhost.y > state->windowHeight + 40) {
        //al_play_sample(state->doorSound, 0.7, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
        playSound(state->doorSound, 0.7, state);
        goOutside(state);
    }
}

bool roomCollisions(GameState* state, enum direction dir, float speed) {
    Room* room = &state->room;
    Entity* ghost = &state->insideGhost;

    float tileSize = state->room.tileSize;
    float roomX = state->room.x;
    float roomY = state->room.y;
    float floorY = state->room.floorY;
    float lowerWallY = roomY + (tileSize*state->room.height);
    float hallwayRightX = room->hallwayX + room->hallwayWidth*room->tileSize;

    switch (dir) {
        case LEFT:
            // hitting left wall
            if (ghost->x + speed < roomX)
                return true;
            else if (ghost->y > lowerWallY - ghost->height &&
                     ghost->x + speed < room->hallwayX)
                return true;
            break;
        case RIGHT:
            // hitting right wall
            if (ghost->x + speed > roomX + tileSize*state->room.width - ghost->width)
                return true;
            else if (ghost->y > lowerWallY - ghost->height &&
                     ghost->x + speed > hallwayRightX - ghost->width)
                return true;
            break;
        case UP:
            // hitting upper wall
            if (ghost->y + speed < floorY-(ghost->height*3/4)) 
                return true;
            break;
        case DOWN:
            // hitting lower wall
            if (ghost->y + speed > lowerWallY - ghost->height &&
                    (ghost->x < room->hallwayX || ghost->x > hallwayRightX - ghost->width))
                return true;
            break;
        default:
            return false;
    }
    return false;
}

void toggleLights(GameState* state) {
    state->room.lightsOn = !state->room.lightsOn;
    if (state->room.lightsOn) {
        state->room.lantern.image = state->images.lanternOn;
    } else {
        state->room.lantern.image = state->images.lanternOff;
    }
}
