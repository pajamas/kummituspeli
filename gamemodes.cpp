#include "structs.h"
#include "save.h"
#include "gamemodes.h"
#include "tetrislogic.h"

void pauseGame(GameState* state) {
    state->previousGameMode = state->gameMode;
    state->gameMode = PAUSED;
    state->canShoot = false;
    state->currentMenu = &state->escMenu;
}

void resumeGame(GameState* state) {
    state->gameMode = state->previousGameMode;
    state->canShoot = true;
    state->currentMenu = NULL;
    if (state->gameMode == TETRIS) {
        al_start_timer(state->tetris->tetrisTimer);
    }
}

void quitGame(GameState* state) {
    save(state);
    state->done = true;
}

void goToSettings(GameState* state) {
    state->currentMenu = &state->settingsMenu;
}

void goToPauseMenu(GameState* state) {
    state->gameMode = PAUSED;
    state->currentMenu = &state->escMenu;
}

void goToControlsMenu(GameState* state) {
    state->currentMenu = &state->controlsMenu;
}
