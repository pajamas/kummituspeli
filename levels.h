#ifndef LEVELS_H_INCLUDED
#define LEVELS_H_INCLUDED

void clearLevel(GameState* state);
void addToLevel(GameState* state, Entity* entity);
void genLevel(int currentLev, GameState* state);
void genNextLevel(GameState* state);
void genPreviousLevel(GameState* state);
void goIntoHouse(GameState* state);
void goIntoGreenhouse(GameState* state);
void goOutside(GameState* state);
void goHome(GameState* state);
void myshaClick(GameState* state);

#endif
