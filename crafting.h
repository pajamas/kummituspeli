#ifndef CRAFTING_H_INCLUDED
#define CRAFTING_H_INCLUDED

void initCraftMenu(GameState* state);
void openCraftingTable(GameState* state);
void closeCraftingTable(GameState* state);
void drawCraftMenu(GameState* state);

#endif
