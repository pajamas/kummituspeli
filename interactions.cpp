#include "structs.h"
#include "inventory.h"
#include "entities.h"
#include "other.h"

void removeFromHand(StackOfItems* handItem, int amount) {
    if (handItem->item == NULL) return;
    if (handItem->amount == 0) return;
    
    handItem->amount -= amount;
    if (handItem->amount <= 0) {
        handItem->item = NULL;
        handItem->amount = 0;
        handItem->notOwned = false;
    }
}

void flowerPotInteract(GameState* state) {
    if ((state->handItem.item == &state->seedItem) && state->flowerPot.growthStage == 0) {
        int speciesNum = rand() % PLANTABLE_SPECIES;
        state->flowerPot.dialogue = "you planted a seed!";

        state->handItem.amount -= 1;
        if (state->handItem.amount == 0) {
            state->handItem.item = NULL;
        }
        
        state->flowerPot.growthStage = 1;
        state->flowerPot.image = state->flowerPotStages[speciesNum][0];
        state->flowerPot.makeSize();
        state->flowerPot.speciesID = speciesNum;
        state->flowerPot.yOffset = -al_get_bitmap_height(state->flowerPot.image);
        state->flowerPot.item = &state->flowerItems[speciesNum];
        al_start_timer(state->growTimer);
    }
    else if (state->flowerPot.growthStage == FLOWER_POT_STAGES_NUM-1) {
        state->flowerPot.harvestable = true;
        state->flowerPot.harvest(state);
        state->flowerPot.image = state->images.emptyFlowerPot;
        state->flowerPot.makeSize();
        state->flowerPot.yOffset = -al_get_bitmap_height(state->flowerPot.image);
        state->flowerPot.growthStage = 0;
        state->flowerPot.dialogue = "";
    }
}

// flower pot growing function (has 1/10 chance of growing)
void grow(GameState* state) {
    if (rand() % 10 != 0) return;
    state->flowerPot.growthStage++;
    if (state->flowerPot.growthStage == FLOWER_POT_STAGES_NUM-1) {
        al_stop_timer(state->growTimer);
    }
    state->flowerPot.image = state->flowerPotStages[state->flowerPot.speciesID]
                                                   [state->flowerPot.growthStage];
    state->flowerPot.yOffset = -al_get_bitmap_height(state->flowerPot.image);

    state->flowerPot.dialogue = "it is plant";
}

void toggleBookOpen(GameState* state) {
    state->openBook.showing = !state->openBook.showing;
    state->bookState.leftPage.showing = !state->bookState.leftPage.showing;                
    state->bookState.rightPage.showing = !state->bookState.rightPage.showing;                
}   

void bookInteract(GameState* state, Entity* page) {
    if (state->handItem.item == NULL || !state->handItem.item->isFlower)
        return;
    
    FOR(PLANTABLE_SPECIES) {
        if (i == state->handItem.item->flowerID && 
                state->bookState.containedInBook[i] == 1) {
            return;
        }
    }

    int pageNum;
    if (page == &state->bookState.leftPage) {
        pageNum = state->bookState.currentPage;
    } else {
        pageNum = state->bookState.currentPage + 1;
    }
    
    state->bookState.pages[pageNum] = state->handItem.item->bookPageImage;
    page->image = state->bookPages[state->handItem.item->flowerID];
    state->bookState.containedInBook[state->handItem.item->flowerID] = 1;
    
    removeFromHand(&state->handItem, 1);
}

void foxInteract(GameState* state) {
    if (state->handItem.item != NULL && state->handItem.item->isFood) {
        state->fox.feed(state->handItem.item, state);
        if (state->fox.asleep) {
            state->fox.asleep = false;
            state->fox.changeImage(state->fox.normalImage);
            state->fox.dialogue = "hehehehehe";
        } else {
            state->fox.fallAsleep();
        }
        return;
    }
    state->fox.showDialogue();    
}
