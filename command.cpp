#include <iostream>
#include "structs.h"
#include "draw.h"
#include "inventory.h"

void openCommandLine(GameState* state) {
    state->previousGameMode = state->gameMode;
    state->gameMode = COMMAND;
    state->canShoot = false;
}

void closeCommandLine(GameState* state) {
    state->gameMode = state->previousGameMode;
    state->canShoot = true;
    state->commandText.clear();
}

void drawCommandLine(GameState* state) {
    drawMode(state, state->previousGameMode);
    int lineHeight = 28;
    al_draw_filled_rectangle(0,state->windowHeight-lineHeight,
                             state->windowWidth, state->windowHeight,
                             al_map_rgba(112,84,126,0.5));
    al_draw_text(state->fonts.fontMedium, state->colors.textColor,
                 10, state->windowHeight-lineHeight + 5, ALLEGRO_ALIGN_LEFT,
                 state->commandText.c_str());
}

void parseCommand(GameState* state) {
    closeCommandLine(state);
    
    std::cout << "'" << state->commandText << "'" << std::endl;

    string command = "";
    FOR(state->commandText.length()-1) {
        if (state->commandText[i] == ' ' || state->commandText[i] == '\n') {
            break;
        }
        command = command + state->commandText[i];
    }

    std::cout << "'" << command << "'" << std::endl;
    string commandOption1 = "give";
    if (command == commandOption1) {
        addToInventory(&state->inventory, &state->seedItem, 100);
    }
}
