#include <iostream>
#include "structs.h"
#include "other.h"

Button newButton(GameState* state, string text) {
    Button button = (Button) {
        .width = (double) al_get_text_width(state->fonts.font, text.c_str()) + state->windowHeight / 11,
        .height = (double) state->windowHeight / 11,
        .bgColor = state->colors.buttonBg,
        .activeBgColor = state->colors.buttonActiveBg,
        .disabledBgColor = state->colors.buttonDisabledBg,
        .text = text,
    };
    button.x = (double) state->windowWidth/2 - button.width/2;
    button.y = (double) state->windowHeight/2 - button.height/2;
    return button;
}

Menu newMenu(GameState* state) {
    Menu menu = (Menu) {
        .x = (double) state->windowWidth / 4,
        .y = (double) state->windowHeight / 4,
        .width = (double) state->windowWidth / 2,
        .height = (double) state->windowHeight / 2,
        .padding = 30,
        .paddingTop = 40,
        .bgColor = state->colors.menuBgColorTransparent,
        .text = "",
    };
    return menu;
}

Inventory newInventory(int width, int height, GameState* state) {
    Inventory inv = (Inventory) {
        .size = width*height,
        .width = width,
        .height = height,
        .padding = 9,
        .slotWidth = 45,
        .bgColor = state->colors.inventoryBg,
        .slotBgColor = state->colors.inventorySlotBg,
    };
    inv.widthPx = (inv.width + 1)*inv.padding + inv.width*inv.slotWidth; 
    inv.heightPx = (inv.height + 1)*inv.padding + inv.height*inv.slotWidth; 
    return inv;
}

void addTextToMenu(Menu* menu, string text) {
    menu->text = text;
    menuTextToRows(menu);
}

void menuTextToRows(Menu* menu) {
    int length = menu->text.length();

    menu->rowsNum = 1;

    int i = 0;
    int rowI = 0;
    int rowTextI = 0;
    char currentChar;

    while (true) {
        currentChar = menu->text[i];
        if (currentChar == '\n') {
            rowI += 1;
            rowTextI = 0;
            i++;
            menu->rowsNum += 1;
            continue;
        }
        if (i == length) {
            break;
        }
        menu->rows[rowI] += currentChar;
        rowTextI++;
        i++;
    }
}

void autoSizeMenu(GameState* state, Menu* menu) {
    int padding = menu->padding;
    int h = menu->paddingTop;

    FOR(menu->rowsNum) {
        h += state->fonts.fontSize;
        h += padding;
    }

    FOR(10) {
        if (menu->buttons[i] != NULL) {
            h += menu->buttons[i]->height;
            h += padding;
        }
    }
    menu->height = h;
    menu->y = (state->windowHeight - h) / 2;
}

void autoSizeButton(GameState* state, Button* button) {
    button->width = al_get_text_width(state->fonts.font, button->text.c_str()) 
                    + state->windowHeight / 11;
}

bool buttonInteract(Button* button, GameState* state) {
    if (checkForMouseOnButton(button)) {
        button->command(state);
        return true;
    }
    return false;
}

// returns true if mouse on button, otherwise false
bool checkForMouseOnButton(Button* button) {
    if (button == NULL) {
        return false;
    }

    ALLEGRO_MOUSE_STATE mState;
    al_get_mouse_state(&mState);
                
    int buttonWidth, buttonHeight;
    buttonWidth = button->width;
    buttonHeight = button->height;

    return (mState.x > button->x &&
            mState.x < button->x + buttonWidth &&
            mState.y > button->y &&
            mState.y < button->y + buttonHeight);
}

void drawButton(GameState* state, Button* button) {
    ALLEGRO_MOUSE_STATE mState;
    al_get_mouse_state(&mState);

    ALLEGRO_COLOR buttonColor;
    // check for button hover, determine background color
    if (checkForMouseOnButton(button)) {
        buttonColor = button->activeBgColor;
    } else {
        buttonColor = button->bgColor;
    }

    if (button->disabled) {
        buttonColor = button->disabledBgColor;
    }

    // draw button
    al_draw_filled_rectangle(button->x, button->y, 
            button->x+button->width, button->y+button->height, 
            buttonColor);
    al_draw_text(state->fonts.font, state->colors.textColor, 
            button->x+button->width/2,button->y+button->height/2-10,
            ALLEGRO_ALIGN_CENTRE, button->text.c_str());

    if (button->image != NULL) {
        al_draw_bitmap(button->image, button->x + button->width/2 
                       - bitmap_width(button->image)/2,
                       button->y + button->height/2 - bitmap_height(button->image)/2,
                       0);
    }
}

void drawMenu(GameState* state, Menu* menu) {
    // BACKGROUND
    al_draw_filled_rectangle(menu->x,menu->y,
                             menu->x+menu->width,menu->y+menu->height, 
                             menu->bgColor );
    // TEXT
    double h1 = menu->y + menu->paddingTop;
    FOR (menu->rowsNum) {
        al_draw_text(state->fonts.font, state->colors.textColor,
                     state->windowWidth/2, h1,
                     ALLEGRO_ALIGN_CENTRE, menu->rows[i].c_str());
        h1 += state->fonts.fontSize + menu->padding;
    }
    double padding = menu->padding;
    double h = menu->y + menu->paddingTop + menu->rowsNum*state->fonts.fontSize + menu->rowsNum*padding;
    // BUTTONS
    FOR(10) {
        if (menu->buttons[i] != NULL) {
            menu->buttons[i]->y = h;
            h += menu->buttons[i]->height + padding;
            drawButton(state, menu->buttons[i]);
        }
    }
}

void createBubble(GameState* state, Entity* ghost) {   
    if (ghost == NULL) return;
    // direction for bubble
    double bubbleStartX = ghost->x +16;
    if (state->gameMode == OUTSIDE) {
        bubbleStartX -= state->viewX;
    }
    double bubbleStartY = ghost->y+5;
            
    ALLEGRO_MOUSE_STATE mouseState;
    al_get_mouse_state(&mouseState);

    if (mouseState.x == bubbleStartX && mouseState.y == bubbleStartY) return;

    Vector2d v = newVector2d(mouseState.x - bubbleStartX,
                             mouseState.y - bubbleStartY);

    v = unitVector2d(v);
    v = vector2dMultiply(v,state->bubbleSpeed);
 
    // define new bubble
    int n = freeBubbleSlot(state->bubbles);
    Bubble newBubble = (Bubble) {
        .x = bubbleStartX,
        .y = bubbleStartY,
        .dx = v.x,
        .dy = v.y,
        .alive = true,
        .image = state->bubbleImg,
    };

    //al_play_sample(state->bubblePop, 0.1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
    playSound(state->bubblePop, 0.1, state);

    state->bubbles[n] = newBubble;
}

int freeBubbleSlot(Bubble* bubbles) {
    FOR(MAX_BUBBLES) {
        if (!bubbles[i].alive) {
            return i;
        }
    }
    FOR(MAX_BUBBLES) {
        bubbles[i-1] = bubbles[i];
    }
    return MAX_BUBBLES-1;
}

void drawBubble(Bubble* bubble) {
    al_draw_bitmap(bubble->image, bubble->x, bubble->y, 0);
}

Vector2d newVector2d(double x, double y) {
    return (Vector2d) {
        .x = x,
        .y = y
    };
}

Vector2d vector2dDivide(Vector2d vector, double num) {
    vector.x /= num;
    vector.y /= num;
    return vector;
}

Vector2d vector2dMultiply(Vector2d vector, double num) {
    vector.x *= num;
    vector.y *= num;
    return vector;
}

double vector2dLength(Vector2d vector) {
    return sqrt(pow(vector.x,2)+pow(vector.y,2));
}

Vector2d unitVector2d(Vector2d vector) {
    return vector2dDivide(vector, vector2dLength(vector));
}

int newItemID(int* newestID) {
    *newestID += 1;
    return *newestID;
}

Item::Item() {
    this->name = "";
    this->image = NULL;
    this->ID = -1;
    this->buyPrice = -1;
    this->sellPrice = -1;
    this->isGem = false;
    this->isFlower = false;
    this->isMineral = false;
    this->nonStackable = false;
    this->onclick = NULL;
}

Item::Item(std::string name, std::string imagePath, GameState* state) {
    this->name = name;
    this->image = al_load_bitmap(imagePath.c_str());
    this->ID = newItemID(&state->newestItemID);
    this->buyPrice = -1;
    this->sellPrice = -1;
    this->isGem = false;
    this->isFlower = false;
    this->isMineral = false;
    this->nonStackable = false;
    this->onclick = NULL;
}

Item::Item(std::string name, ALLEGRO_BITMAP* image, GameState* state) {
    this->name = name;    
    this->image = image;    
    this->ID = newItemID(&state->newestItemID);    
    this->buyPrice = -1;
    this->sellPrice = -1;
    this->isGem = false;
    this->isFlower = false;
    this->isMineral = false;
    this->nonStackable = false;
    this->onclick = NULL;
}

void Item::setPrices(int buyPrice, int sellPrice) {
    if (buyPrice < -1 || sellPrice < -1) {
        error("item " + this->name + ": invalid buy or sell price");
    }
    this->buyPrice = buyPrice;
    this->sellPrice = sellPrice;
}
