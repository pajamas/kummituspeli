#ifndef EVENTS_H_INCLUDED
#define EVENTS_H_INCLUDED

#include "structs.h"

void handleEvents(GameState* state);
void whatClicked(GameState* state);
void exitGame(GameState* state);

#endif
