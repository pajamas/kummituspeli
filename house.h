#ifndef HOUSE_H_INCLUDED
#define HOUSE_H_INCLUDED

#include "structs.h"

void moveInRoom(GameState* state);
bool roomCollisions(GameState* state, enum direction dir, float speed);
void toggleLights(GameState* state);

#endif
