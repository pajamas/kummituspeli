#include "structs.h"
#include "crafting.h"

void initCraftMenu(GameState* state) {
    CraftMenu* menu = &state->craftMenu;
    menu->x = 100;
    menu->y = 100;
    menu->width = 500;
    menu->height = 300;
    menu->bgColor = state->colors.menuBgColorTransparent;
}

void openCraftingTable(GameState* state) {
    state->chest.open = false;
    state->craftMenu.open = true;
    
}

void closeCraftingTable(GameState* state) {
    state->craftMenu.open = false;
    
}

void drawCraftMenu(GameState* state) {
    CraftMenu* menu = &state->craftMenu;
    al_draw_filled_rectangle(menu->x, menu->y, 
                             menu->x + menu->width, menu->y + menu->height,
                             menu->bgColor);
    // title
    al_draw_text(state->fonts.font, state->colors.textColor, menu->x + menu->padding,
                 menu->y + menu->paddingTop, ALLEGRO_ALIGN_LEFT, "Crafting");
}

